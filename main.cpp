#include "includes.hpp"
#include "overwatch_sdk.hpp"
#include "overwatch.hpp"
#include "scanner.hpp"


void MainThread() {
	if (!SDK->Initialize() || !SDK->GetGlobalKey())
		exit(0);
	K::vmm.Init(Memory::_hVMM);
	std::cout << skCrypt("\n\Welcome!\n\n").decrypt();
     
    _beginthread((_beginthread_proc_type)entity_scan_thread, 0, 0);
    _beginthread((_beginthread_proc_type)entity_thread, 0, 0);
    _beginthread((_beginthread_proc_type)aimbot_thread, 0, 0);
    _beginthread((_beginthread_proc_type)overlay_thread, 0, 0);
	// _beginthread((_beginthread_proc_type)velocity_thread, 0, 0);

	using namespace OW;
	time_t last_vm_update{};
    while (true)
    {
		viewMatrixVal = SDK->RPM<uint64_t>(SDK->dwGameBase + offset::Address_viewmatrix_base) ^ offset::offset_viewmatrix_xor_key;
		__try {
			auto tick = GetTickCount64();
			if ((tick - last_vm_update) > 1000 * 5)
			{
				viewMatrixVal = SDK->RPM<uint64_t>(viewMatrixVal + 0x5C0);
				viewMatrixVal = SDK->RPM<uint64_t>(viewMatrixVal + 0x118);
				viewMatrix_xor_ptr = viewMatrixVal + 0x130;
				viewMatrixPtr = SDK->RPM<uint64_t>(SDK->dwGameBase + offset::Address_viewmatrix_base_test) + offset::offset_viewmatrix_ptr;
				last_vm_update = tick;
			}

			auto hScatter = SDK->hScatterVM;
			Memory::ScatterClear(SDK->dwPID, hScatter, 1);
			Memory::ScatterPrepare(hScatter, viewMatrixVal + 0x424, sizeof(Vector2));
			Memory::ScatterPrepare(hScatter, viewMatrixPtr, sizeof(Matrix));
			Memory::ScatterPrepare(hScatter, viewMatrix_xor_ptr, sizeof(Matrix));
			Memory::ExecuteRead(hScatter);

			Vector2 WindowSize = SDK->ScatterRPM<Vector2>(viewMatrixVal + 0x424, hScatter);
			width = WindowSize.X;
			height = WindowSize.Y;

			viewMatrix = SDK->ScatterRPM<Matrix>(viewMatrixPtr, hScatter);
			viewMatrix_xor = SDK->ScatterRPM<Matrix>(viewMatrix_xor_ptr, hScatter);
			viewMatrixTo = SDK->ScatterRPM<MatrixTo>(viewMatrixPtr, hScatter);
			Sleep(5);
		}
		__except (1) {
		}
    }
}


int main() {
	{
		system(skCrypt("cls").decrypt());
		MainThread();
	}
}