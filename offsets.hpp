#pragma once

namespace OW {
	namespace offset {
		// matrix
		uint64_t Address_viewmatrix_base = 0x3719D98; // xor updated
		uint64_t offset_viewmatrix_ptr = 0x7E0; // updated
		uint64_t Address_viewmatrix_base_test = 0x3DC8838; // updated
		uint64_t offset_viewmatrix_xor_key = 0xA9C94599A097F2AB; // xor updated

		// entity list
		uint64_t Address_entity_list = 0x3705300; // updated

		// visibility
		uint64_t Visible_Fn = 0x782FF2; // updated
		uint64_t Visible_Key = 0x2C8D360FBC5A1771; // updated

		// outline
		uint64_t Outline_Fn = 0x78C622; // updated
		uint64_t Outline_Key = 0x5F7A5A3D53184608; // updated
	}
}