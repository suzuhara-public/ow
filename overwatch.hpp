﻿#pragma once
#include "aimbot.hpp"
#include "bytes.hpp"
#include "settings.hpp"

namespace OW {

	inline void entity_scan_thread() {
		time_t last_entity_update = 0;
		while (true) {
			ow_entities = get_ow_entities();
			Sleep(1000 * 2);
		}
	}

	inline void entity_thread() {
		time_t last_update = 0;
		while (true) {
			time_t tick = GetTickCount64();
			if (tick - last_update > 1000 * 3.5) {
				if (ow_entities.size() > 0) {
					std::vector<c_entity> tmp_entities{};
					for (int i = 0; i < ow_entities.size(); i++) {
						c_entity entity{};
						auto [ComponentParent, LinkParent] = ow_entities[i];
						if (!LinkParent || !ComponentParent)
							continue;

						entity.address = ComponentParent;
						entity.TeamBase = DecryptComponent(ComponentParent, TYPE_TEAM);

						if (entity.TeamBase) {
							eTeam team = entity.GetTeam();
							entity.Team = (team == eTeam::TEAM_DEATHMATCH || team != local_entity.GetTeam()) ? true : false;

							// if (!ParentContainsComponent(LinkParent, TYPE_PLAYERCONTROLLER)) {
							// 	if (!entity.Team) continue;
							// }
						}

						entity.HealthBase = DecryptComponent(ComponentParent, TYPE_HEALTH);
						entity.LinkBase = DecryptComponent(LinkParent, TYPE_LINK);
						entity.VelocityBase = DecryptComponent(ComponentParent, TYPE_VELOCITY);
						entity.HeroBase = DecryptComponent(LinkParent, TYPE_P_HEROID);
						entity.OutlineBase = DecryptComponent(ComponentParent, TYPE_OUTLINE);
						entity.RotationBase = DecryptComponent(ComponentParent, TYPE_ROTATION);
						entity.SkillBase = DecryptComponent(ComponentParent, TYPE_SKILL);
						entity.VisBase = DecryptComponent(LinkParent, TYPE_P_VISIBILITY);

						if (entity.OutlineBase) {
							if (settings::wallhack::outline_players) {
								uint64_t OutlineStruct = entity.OutlineBase + 0x20;
								entity.BorderStruct = GetOutlineStruct(OutlineStruct);
								entity.DecryptData = DecryptOutline(SDK->RPM<uint64_t>(entity.BorderStruct + 0x18));
								entity.ColorStruct = GetColorStruct(OutlineStruct);
							}
						}

						const auto angle_component = DecryptComponent(LinkParent, TYPE_PLAYERCONTROLLER);
						if (angle_component)
						{
							entity.isLocal = true;
							SDK->g_player_controller = angle_component;
						}
						if (ComponentParent)
							tmp_entities.push_back(entity);
					}
					entities = tmp_entities;
				}
				else {
					entities.clear();
				}
				last_update = tick;
			}

			auto hScatter = SDK->hScatterProp;
			for (auto& entity : entities)
			{
				Memory::ScatterClear(SDK->dwPID, hScatter, 1);
				// 分散读
				{
					if (entity.HealthBase) {
						Memory::ScatterPrepare(hScatter, entity.HealthBase + 0xE0, sizeof(float));
						Memory::ScatterPrepare(hScatter, entity.HealthBase + 0xDC, sizeof(float));
						Memory::ScatterPrepare(hScatter, entity.HealthBase + 0x220, sizeof(float));
						Memory::ScatterPrepare(hScatter, entity.HealthBase + 0x21C, sizeof(float));
						Memory::ScatterPrepare(hScatter, entity.HealthBase + 0x360, sizeof(float));
						Memory::ScatterPrepare(hScatter, entity.HealthBase + 0x35C, sizeof(float));
					}
					if (entity.RotationBase) {
						auto Base = SDK->RPM<uint64_t>(entity.RotationBase + 0x7D0, false);
						Memory::ScatterPrepare(hScatter, Base + 0x35C, sizeof(Vector3));
					}

					if (entity.VelocityBase) {
						Memory::ScatterPrepare(hScatter, entity.VelocityBase + 0x50, sizeof(XMFLOAT3));
						Memory::ScatterPrepare(hScatter, entity.VelocityBase + 0x200, sizeof(XMFLOAT3));
					}

					if (entity.HeroBase) {
						Memory::ScatterPrepare(hScatter, entity.HeroBase + 0xD0, sizeof(uint64_t));
					}

					if (entity.VisBase) {
						Memory::ScatterPrepare(hScatter, entity.VisBase + 0xA0, sizeof(uint64_t));
						Memory::ScatterPrepare(hScatter, entity.VisBase + 0x98, sizeof(uint64_t));
					}

					/*if (entity.TeamBase) {
						Memory::ScatterPrepare(hScatterProp, entity.TeamBase + 0x58, sizeof(uint32_t));
					}*/

					Memory::ExecuteRead(hScatter);
				}
				if (entity.HealthBase) {
					float health = SDK->ScatterRPM<float>(entity.HealthBase + 0xE0, hScatter);
					float health_max = SDK->ScatterRPM<float>(entity.HealthBase + 0xDC, hScatter);
					float armor = SDK->ScatterRPM<float>(entity.HealthBase + 0x220, hScatter);
					float armor_max = SDK->ScatterRPM<float>(entity.HealthBase + 0x21C, hScatter);
					float barrier = SDK->ScatterRPM<float>(entity.HealthBase + 0x360, hScatter);
					float barrier_max = SDK->ScatterRPM<float>(entity.HealthBase + 0x35C, hScatter);

					entity.PlayerHealth = health + armor + barrier;
					entity.PlayerHealthMax = health_max + armor_max + barrier_max;
					entity.MinHealth = health;
					entity.MaxHealth = health_max;
					entity.MinArmorHealth = armor;
					entity.MaxArmorHealth = armor_max;
					entity.MinBarrierHealth = barrier;
					entity.MaxBarrierHealth = barrier_max;
					entity.Alive = (entity.PlayerHealth > 0.f) ? true : false;
				}

				if (entity.RotationBase) {
					auto Base = SDK->RPM<uint64_t>(entity.RotationBase + 0x7D0, false);
					entity.Rot = SDK->ScatterRPM<Vector3>(Base + 0x8FC, hScatter);
				}

				if (entity.VelocityBase) {
					XMFLOAT3 velocity = SDK->ScatterRPM<XMFLOAT3>(entity.VelocityBase + 0x50, hScatter);
					XMFLOAT3 location = SDK->ScatterRPM<XMFLOAT3>(entity.VelocityBase + 0x200, hScatter);
					entity.pos = Vector3(location.x, location.y - 1.f, location.z);
					entity.velocity = Vector3(velocity.x, velocity.y, velocity.z);
					static int head_index = entity.GetSkel()[0];
					entity.head_pos = entity.GetBonePos(head_index);
				}

				if (entity.HeroBase) {
					uint64_t heroid = SDK->ScatterRPM<uint64_t>(entity.HeroBase + 0xD0, hScatter);
					entity.HeroID = heroid;
				}

				if (entity.VisBase) {
					uint64_t key1 = SDK->ScatterRPM<uint64_t>(entity.VisBase + 0xA0, hScatter);
					uint64_t key2 = SDK->ScatterRPM<uint64_t>(entity.VisBase + 0x98, hScatter);
					entity.Vis = (DecryptVis(key1) ^ key2) ? true : false;
				}

				if (entity.OutlineBase) {
					if (settings::wallhack::outline_players) {
						uint64_t BorderStruct = entity.BorderStruct;
						uint64_t DecryptData = entity.DecryptData;
						uint64_t ColorStruct = entity.ColorStruct;

						if (entity.Team) {
							if (entity.Vis) {
								SDK->WPM<uint32_t>(BorderStruct + 0x10, DecryptData ^ 1);
								SDK->WPM<uint32_t>(ColorStruct + 0x10, settings::color::visible_color);
							}
							else {
								SDK->WPM<uint32_t>(BorderStruct + 0x10, DecryptData ^ 2);
								SDK->WPM<uint32_t>(ColorStruct + 0x10, settings::color::invisible_color);
							}
						}
						else {
							SDK->WPM<uint32_t>(BorderStruct + 0x10, DecryptData ^ 2);
							SDK->WPM<uint32_t>(ColorStruct + 0x10, settings::color::team_color);
						}
					}
				}

				if (entity.isLocal)
					local_entity = entity;
			}
			
			// Sleep(5);
		}
	}

	inline void velocity_thread() {
		while (true) {
			if (entities.size() > 0) {
				for (int i = 0; i < entities.size(); i++) {
					//if (entities[i].Team && entities[i].Alive) 
					{
						clock_t currTime = clock();
						if ((currTime - entities[i].lastVelocityUpdate) >= 20)
						{
							entities[i].velocity.X = (entities[i].pos.X - entities[i].lastpos.X) / (currTime - entities[i].lastVelocityUpdate);
							entities[i].velocity.Y = (entities[i].pos.Y - entities[i].lastpos.Y) / (currTime - entities[i].lastVelocityUpdate);
							entities[i].velocity.Z = (entities[i].pos.Z - entities[i].lastpos.Z) / (currTime - entities[i].lastVelocityUpdate);
							entities[i].lastpos = entities[i].pos;

							entities[i].lastVelocityUpdate = currTime;
						}
					}
				}
				Sleep(1);
			}
		}
	}

	inline void DrawBox(bool overwatch, ImColor color = ImColor(255, 255, 255, 255)) {
		if (!overwatch) return;

		if (entities.size() > 0) {
			for (c_entity entity : entities) {
				if (entity.Alive && entity.Team && local_entity.PlayerHealth > 0)
				{
					if (settings::wallhack::only_visible) {
						if (!entity.Vis) continue;
					}
					if (settings::wallhack::visible_check) {
						color = entity.Vis ? settings::color::visible_color : settings::color::invisible_color;
					}

					Vector3 Vec3 = entity.head_pos;
					Vector2 Vec2_A{}, Vec2_B{};
					if (!viewMatrix.WorldToScreen(Vector3(Vec3.X, Vec3.Y + 0.1f, Vec3.Z), &Vec2_A, Vector2(width, height)))
						continue;

					if (!viewMatrix.WorldToScreen(Vector3(Vec3.X, Vec3.Y - 1.8f, Vec3.Z), &Vec2_B, Vector2(width, height)))
						continue;

					float height = abs(Vec2_A.Y - Vec2_B.Y);
					float width = height * 0.85;
					float size = abs(Vec2_A.Y - Vec2_B.Y) / 2.0f;

					if (settings::wallhack::box_type == 0) {
						draw::Draw2DBox(Vec2_A.X - (width / 2), Vec2_A.Y - (height / 5), width, height, color);
					}
					else {
						draw::DrawCorneredBox(Vec2_A.X - (width / 2), Vec2_A.Y - (height / 5), width, height, color);
					}
				}
			}
		}
	}

	inline void DrawDistance(bool overwatch, ImColor color = ImColor(255, 255, 255, 255)) {
		if (!overwatch) return;

		if (entities.size() > 0) {
			for (c_entity entity : entities) {
				if (entity.Alive && entity.Team && local_entity.PlayerHealth > 0)
				{
					if (settings::wallhack::only_visible) {
						if (!entity.Vis) continue;
					}
					if (settings::wallhack::visible_check) {
						color = entity.Vis ? settings::color::visible_color : settings::color::invisible_color;
					}

					Vector3 enemy_pos = entity.pos;

					Vector2 Vec2_A{}, Vec2_B{};
					if (!viewMatrix.WorldToScreen(Vector3(enemy_pos.X, enemy_pos.Y + 1.f, enemy_pos.Z), &Vec2_A, Vector2(width, height)))
						continue;

					if (!viewMatrix.WorldToScreen(Vector3(enemy_pos.X, enemy_pos.Y - 0.8f, enemy_pos.Z), &Vec2_B, Vector2(width, height)))
						continue;

					auto distance = "[" + std::to_string((int)viewMatrix.GetCameraVec().Distance(enemy_pos)) + "m]";
					auto textsize = ImGui::CalcTextSize(distance.c_str());

					if (settings::wallhack::health) {
						draw::Text(ImVec2(Vec2_A.X - textsize.x / 2, Vec2_B.Y - ((textsize.y / 2) - 10)), color, distance.c_str());
					}
					else {
						draw::Text(ImVec2(Vec2_A.X - textsize.x / 2, Vec2_B.Y - textsize.y / 2), color, distance.c_str());
					}
				}
			}
		}
	}

	inline void DrawHeroName(bool overwatch, ImColor color = ImColor(255, 255, 255, 255)) {
		if (!overwatch) return;

		if (entities.size() > 0) {
			for (c_entity entity : entities) {
				if (entity.Alive && entity.Team && local_entity.PlayerHealth > 0)
				{
					if (settings::wallhack::only_visible) {
						if (!entity.Vis) continue;
					}
					if (settings::wallhack::visible_check) {
						color = entity.Vis ? settings::color::visible_color : settings::color::invisible_color;
					}

					Vector3 enemy_pos = entity.pos;

					Vector2 Vec2_A{}, Vec2_B{};
					if (!viewMatrix.WorldToScreen(Vector3(enemy_pos.X, enemy_pos.Y + 1.f, enemy_pos.Z), &Vec2_A, Vector2(width, height)))
						continue;

					if (!viewMatrix.WorldToScreen(Vector3(enemy_pos.X, enemy_pos.Y - 0.8f, enemy_pos.Z), &Vec2_B, Vector2(width, height)))
						continue;

					auto hero = GetHeroNames(entity.HeroID, entity.LinkBase);
					auto textsize = ImGui::CalcTextSize(hero.c_str());

					draw::Text(ImVec2(Vec2_A.X - textsize.x / 2, Vec2_B.Y - ((textsize.y / 2) + 14)), color, hero.c_str());
				}
			}
		}
	}

	inline void DrawHealth(bool overwatch) {
		if (!overwatch) return;

		if (entities.size() > 0) {
			for (c_entity entity : entities) {
				if (entity.Alive && entity.Team && local_entity.PlayerHealth > 0)
				{
					if (settings::wallhack::only_visible) {
						if (!entity.Vis) continue;
					}

					Vector3 Vec3 = entity.pos;

					Vector2 Vec2_A{}, Vec2_B{};
					if (!viewMatrix.WorldToScreen(Vector3(Vec3.X, Vec3.Y + 0.1f, Vec3.Z), &Vec2_A, Vector2(width, height)))
						continue;

					if (!viewMatrix.WorldToScreen(Vector3(Vec3.X, Vec3.Y - 1.8f, Vec3.Z), &Vec2_B, Vector2(width, height)))
						continue;

					float Size = abs(Vec2_A.Y - Vec2_B.Y) / 2.0f;
					float xpos = (Vec2_A.X + Vec2_B.X) / 2;
					float ypos = Vec2_A.Y + Size;
					int barSize = ypos - ypos;
					int TextOffset = 0;

					if (barSize <= 60) {
						barSize = 60;
						TextOffset += 10;
					}
					if (barSize > 60 && barSize <= 160) {
						barSize = 120;
						TextOffset += 20;
					}

					if (barSize > 160) {
						barSize = 200;
						TextOffset += 30;
					}

					draw::DrawHealthBar(barSize, ImVec2(xpos - (barSize / 2), ypos), entity.PlayerHealth, entity.PlayerHealthMax, entity.MinArmorHealth, entity.MaxArmorHealth, entity.MinBarrierHealth, entity.MaxBarrierHealth);
				}
			}
		}
	}

	inline void AutoHeal(bool overwatch) {
		if (!overwatch) return;

		bool healed = false;
		if (!healed) {
			if (local_entity.PlayerHealth < settings::misc::min_health) {
				Sendinput::SendVKcodes(0x45);
				healed = true;
			}
		}
	}

	inline void AutoMelee(bool overwatch) {
		if (!overwatch) return;

		if (entities.size() > 0) {
			for (c_entity entity : entities) {
				if (local_entity.Alive && entity.Team && local_entity.PlayerHealth > 0)
				{
					Vector3 enemy_pos = entity.head_pos;
					float dist = viewMatrix.GetCameraVec().Distance(enemy_pos);

					bool meleed = false;
					if (dist < settings::misc::min_dist) {
						if (!meleed) {
							Sendinput::SendVKcodes(settings::misc::meleekey);
							meleed = true;
						}
					}
				}
			}
		}
	}

	inline void test(bool overwatch) {
		if (!overwatch) return;

		for (c_entity entity : entities) {
			for (int i = 0; i < entities.size(); i++) {
				if (entities[i].Team && entities[i].Alive) {

					Vector2 Vec2_Q = viewMatrix.WorldToScreen(entities[i].predict);

					Vector3 Vec3 = entities[i].pos;

					Vector2 Vec2_A{}, Vec2_B{};
					if (!viewMatrix.WorldToScreen(Vector3(Vec3.X, Vec3.Y + 0.1f, Vec3.Z), &Vec2_A, Vector2(width, height)))
						continue;

					if (!viewMatrix.WorldToScreen(Vector3(Vec3.X, Vec3.Y - 1.8f, Vec3.Z), &Vec2_B, Vector2(width, height)))
						continue;

					auto distance = "[" + std::to_string((int)viewMatrix.GetCameraVec().Distance(Vec3)) + "m]";
					auto textsize = ImGui::CalcTextSize(distance.c_str());
					float test = 1.0f;

					draw::DrawBar(ImVec2(Vec2_A.X, Vec2_A.Y), 30, distance);
				}
			}
		}
	}

	inline void esp() {
		__try
		{
			DrawBox(settings::wallhack::box);
			DrawDistance(settings::wallhack::distance);
			DrawHeroName(settings::wallhack::heroname);
			DrawHealth(settings::wallhack::health);
			AutoHeal(settings::misc::auto_heal);
			AutoMelee(settings::misc::auto_melee);
		}
		__except (1) {

		}
	}

	void ChangeClickability(HWND MyWnd, bool ShowMenu)
	{
		long style = GetWindowLong(MyWnd, GWL_EXSTYLE);
		if (ShowMenu) {
			style &= ~WS_EX_LAYERED;
			SetWindowLong(MyWnd, GWL_EXSTYLE, style);
		}
		else {
			style |= WS_EX_LAYERED;
			SetWindowLong(MyWnd, GWL_EXSTYLE, style);
		}
	}

	inline void overlay_thread() {
		__try {
			std::chrono::system_clock::time_point a = std::chrono::system_clock::now();
			std::chrono::system_clock::time_point b = std::chrono::system_clock::now();

			// HWND tWnd = FindWindowA(skCrypt("TankWindowClass"), NULL);
			float w = GetSystemMetrics(SM_CXSCREEN);
			float h = GetSystemMetrics(SM_CYSCREEN);

			WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, skCrypt("Chrome"), NULL };
			RegisterClassEx(&wc);
			HWND hwnd = CreateWindow(wc.lpszClassName, skCrypt("Chrome"), WS_POPUP, 0, 0, w, h, NULL, NULL, wc.hInstance, NULL);

			if (!CreateDeviceD3D(hwnd))
			{
				CleanupDeviceD3D();
				UnregisterClass(wc.lpszClassName, wc.hInstance);
			}

			MARGINS margins = { -1 };
			DwmExtendFrameIntoClientArea(hwnd, &margins);

			SetWindowLongPtr(hwnd, GWL_STYLE, WS_VISIBLE);
			SetWindowLongPtr(hwnd, GWL_EXSTYLE, WS_EX_LAYERED | WS_EX_TRANSPARENT | WS_EX_NOACTIVATE);

			// SetWindowPos(hwnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
			// SetWindowLong(hwnd, GWL_EXSTYLE, WS_EX_LAYERED | WS_EX_TRANSPARENT | WS_EX_NOACTIVATE);

			ShowWindow(hwnd, SW_SHOWDEFAULT);
			UpdateWindow(hwnd);

			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			SetForegroundWindow(hwnd);
			ChangeClickability(hwnd, settings::defines::menu);

			ImVec4* colors = ImGui::GetStyle().Colors;
			colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
			colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
			colors[ImGuiCol_WindowBg] = ImVec4(0.10f, 0.10f, 0.10f, 1.00f);
			colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			colors[ImGuiCol_PopupBg] = ImVec4(0.19f, 0.19f, 0.19f, 0.92f);
			colors[ImGuiCol_Border] = ImVec4(0.19f, 0.19f, 0.19f, 0.29f);
			colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.24f);
			colors[ImGuiCol_FrameBg] = ImVec4(0.05f, 0.05f, 0.05f, 0.54f);
			colors[ImGuiCol_FrameBgHovered] = ImVec4(0.19f, 0.19f, 0.19f, 0.54f);
			colors[ImGuiCol_FrameBgActive] = ImVec4(0.20f, 0.22f, 0.23f, 1.00f);
			colors[ImGuiCol_TitleBg] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_TitleBgActive] = ImVec4(0.06f, 0.06f, 0.06f, 1.00f);
			colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
			colors[ImGuiCol_ScrollbarBg] = ImVec4(0.05f, 0.05f, 0.05f, 0.54f);
			colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.34f, 0.34f, 0.34f, 0.54f);
			colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.40f, 0.40f, 0.40f, 0.54f);
			colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.56f, 0.56f, 0.56f, 0.54f);

			colors[ImGuiCol_CheckMark] = ImVec4(ImColor(173, 250, 37, 255));

			colors[ImGuiCol_SliderGrab] = ImVec4(0.34f, 0.34f, 0.34f, 0.54f);
			colors[ImGuiCol_SliderGrabActive] = ImVec4(0.56f, 0.56f, 0.56f, 0.54f);
			colors[ImGuiCol_Button] = ImVec4(0.05f, 0.05f, 0.05f, 0.54f);
			colors[ImGuiCol_ButtonHovered] = ImVec4(0.19f, 0.19f, 0.19f, 0.54f);
			colors[ImGuiCol_ButtonActive] = ImVec4(ImColor(173, 250, 37, 255));
			colors[ImGuiCol_Header] = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_HeaderHovered] = ImVec4(0.00f, 0.00f, 0.00f, 0.36f);
			colors[ImGuiCol_HeaderActive] = ImVec4(0.20f, 0.22f, 0.23f, 0.33f);
			colors[ImGuiCol_Separator] = ImVec4(0.28f, 0.28f, 0.28f, 0.29f);
			colors[ImGuiCol_SeparatorHovered] = ImVec4(0.44f, 0.44f, 0.44f, 0.29f);
			colors[ImGuiCol_SeparatorActive] = ImVec4(0.40f, 0.44f, 0.47f, 1.00f);
			colors[ImGuiCol_ResizeGrip] = ImVec4(0.28f, 0.28f, 0.28f, 0.29f);
			colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.44f, 0.44f, 0.44f, 0.29f);
			colors[ImGuiCol_ResizeGripActive] = ImVec4(0.40f, 0.44f, 0.47f, 1.00f);
			colors[ImGuiCol_Tab] = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TabHovered] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
			colors[ImGuiCol_TabActive] = ImVec4(0.20f, 0.20f, 0.20f, 0.36f);
			colors[ImGuiCol_TabUnfocused] = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TabUnfocusedActive] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
			colors[ImGuiCol_PlotLines] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotHistogram] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_TableHeaderBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TableBorderStrong] = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TableBorderLight] = ImVec4(0.28f, 0.28f, 0.28f, 0.29f);
			colors[ImGuiCol_TableRowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			colors[ImGuiCol_TableRowBgAlt] = ImVec4(1.00f, 1.00f, 1.00f, 0.06f);
			colors[ImGuiCol_TextSelectedBg] = ImVec4(0.20f, 0.22f, 0.23f, 1.00f);
			colors[ImGuiCol_DragDropTarget] = ImVec4(0.33f, 0.67f, 0.86f, 1.00f);
			colors[ImGuiCol_NavHighlight] = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 0.00f, 0.00f, 0.70f);
			colors[ImGuiCol_NavWindowingDimBg] = ImVec4(1.00f, 0.00f, 0.00f, 0.20f);
			colors[ImGuiCol_ModalWindowDimBg] = ImVec4(1.00f, 0.00f, 0.00f, 0.35f);

			ImGuiStyle& style = ImGui::GetStyle();
			style.WindowPadding = ImVec2(8.00f, 8.00f);
			style.FramePadding = ImVec2(5.00f, 2.00f);
			style.CellPadding = ImVec2(6.00f, 6.00f);
			style.ItemSpacing = ImVec2(6.00f, 6.00f);
			style.ItemInnerSpacing = ImVec2(6.00f, 6.00f);
			style.TouchExtraPadding = ImVec2(0.00f, 0.00f);
			style.IndentSpacing = 25;
			style.ScrollbarSize = 15;
			style.GrabMinSize = 10;
			style.WindowBorderSize = 1;
			style.ChildBorderSize = 1;
			style.PopupBorderSize = 1;
			style.FrameBorderSize = 1;
			style.TabBorderSize = 1;
			style.WindowRounding = 7;
			style.ChildRounding = 4;
			style.FrameRounding = 3;
			style.PopupRounding = 4;
			style.ScrollbarRounding = 9;
			style.GrabRounding = 3;
			style.LogSliderDeadzone = 4;
			style.TabRounding = 4;
			style.WindowTitleAlign = ImVec2(0.5f, 0.5f);

			ImGui_ImplWin32_Init(hwnd);
			ImGui_ImplDX11_Init(g_pd3dDevice, g_pd3dDeviceContext);

			ImGui::GetIO().Fonts->AddFontFromMemoryTTF(font, sizeof(font), 16.f);
			logo = ImGui::GetIO().Fonts->AddFontFromMemoryTTF(font, sizeof(font), 35.f);

			MSG msg;
			ZeroMemory(&msg, sizeof(msg));
			int FPS;

			while (true)
			{
				DEVMODE dm;
				dm.dmSize = sizeof(DEVMODE);

				EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &dm);

				FPS = dm.dmDisplayFrequency;

				a = std::chrono::system_clock::now();
				std::chrono::duration<double, std::milli> work_time = a - b;
				if (work_time.count() < 1000 / FPS)
				{
					std::chrono::duration<double, std::milli> delta_ms(1000 / FPS - work_time.count());
					auto delta_ms_duration = std::chrono::duration_cast<std::chrono::milliseconds>(delta_ms);
					std::this_thread::sleep_for(std::chrono::milliseconds(delta_ms_duration.count()));
				}

				b = std::chrono::system_clock::now();
				std::chrono::duration<double, std::milli> sleep_time = b - a;

				DWORD Style = GetWindowLong(hwnd, GWL_STYLE);
				RECT rect;
				GetWindowRect(hwnd, &rect);

				SetWindowPos(hwnd, HWND_TOPMOST, rect.left, rect.top, rect.right, rect.bottom, SWP_NOZORDER);
				SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);

				if (::PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
					continue;
				}

				if (GetAsyncKeyState(VK_LBUTTON))
				{
					ImGui::GetIO().MouseDown[0] = true;
				}
				else
				{
					ImGui::GetIO().MouseDown[0] = false;
				}

				if (GetAsyncKeyState(settings::defines::menukey)) {
					settings::defines::menu = !settings::defines::menu;
					ChangeClickability(hwnd, settings::defines::menu);
					Sleep(250);
				}

				if (GetAsyncKeyState(settings::wallhack::hotkey)) {
					settings::wallhack::toggled = !settings::wallhack::toggled;
					Sleep(250);
				}

				// Start the Dear ImGui frame
				ImGui_ImplDX11_NewFrame();
				ImGui_ImplWin32_NewFrame();
				ImGui::NewFrame();

				if (settings::wallhack::fov) ImGui::GetBackgroundDrawList()->AddCircle(ImVec2(width / 2.0f, height / 2.0f), settings::aimbot::fov, 0xFFFFFFFF, 100);

				menu::run();

				esp();
				ImGui::EndFrame();
				ImGui::Render();
				g_pd3dDeviceContext->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
				auto ss = ImVec4(0, 0, 0, 0);\
				g_pd3dDeviceContext->ClearRenderTargetView(g_mainRenderTargetView, (float*)&ss);
				ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

				g_pSwapChain->Present(1, 0);
			}

			// Cleanup
			ImGui_ImplDX11_Shutdown();
			ImGui_ImplWin32_Shutdown();
			ImGui::DestroyContext();

			CleanupDeviceD3D();
			::DestroyWindow(hwnd);
			::UnregisterClass(wc.lpszClassName, wc.hInstance);
		}
		__except (1) {

		}
	}

	XMFLOAT3 MyXMAngle;

	inline void aimbot_thread()
	{
		__try {
			Vector2 CrossHair = Vector2(width / 2.0f, height / 2.0f);
			while (true) {
				//causes slight mouse gittering
				//static float origin_sens = 0.f;
				//if (SDK->RPM<float>(GetSenstivePTR()))
				//	origin_sens = SDK->RPM<float>(GetSenstivePTR());
				//else if (origin_sens)		
				//	SDK->WPM<float>(GetSenstivePTR(), origin_sens);

				bool shooted = false;

				// 自动判断是否是半藏
				bool isHanzo = local_entity.HeroID == eHero::HERO_HANZO;
				settings::aimbot::hanzo_flick = isHanzo;

				//半藏不需要自瞄， aimbot
				if (!isHanzo && settings::aimbot::aimbot) {
					while (K::vmm.IsKeyDown(settings::aimbot::aimkey)) {
						auto vec = GetVector3(settings::aimbot::prediction ? true : false);
						//auto vec = GetVector3Predict();
						if (vec != Vector3(0, 0, 0)) {
							auto local_angle = SDK->RPM<Vector3>(SDK->g_player_controller + 0x12A0); // our angle
							auto calc_target = CalcAngle(XMFLOAT3(vec.X, vec.Y, vec.Z), XMFLOAT3(viewMatrix.GetCameraVec().X, viewMatrix.GetCameraVec().Y, viewMatrix.GetCameraVec().Z));
							auto vec_calc_target = Vector3(calc_target.x, calc_target.y, calc_target.z);

							auto Target = SmoothLinear(local_angle, vec_calc_target, settings::aimbot::aimbot_smooth / 10.f);
							auto local_loc = viewMatrix.GetCameraVec();

							if (Target != Vector3(0, 0, 0)) {
								SDK->WPM<Vector3>(SDK->g_player_controller + 0x12A0, Target);
							}
						}
						Sleep(1);
					}
				}

				//flick
				if (settings::aimbot::flick) {
					while (K::vmm.IsKeyDown(settings::aimbot::flickkey))
					{
						if (!shooted) {
							auto vec = GetVector3(settings::aimbot::prediction ? true : false);
							if (vec != Vector3(0, 0, 0)) {
								auto local_angle = SDK->RPM<Vector3>(SDK->g_player_controller + 0x12A0);
								auto calc_target = CalcAngle(XMFLOAT3(vec.X, vec.Y, vec.Z), XMFLOAT3(viewMatrix.GetCameraVec().X, viewMatrix.GetCameraVec().Y, viewMatrix.GetCameraVec().Z));
								auto vec_calc_target = Vector3(calc_target.x, calc_target.y, calc_target.z);
								auto Target = SmoothAccelerate(local_angle, vec_calc_target, settings::aimbot::flick_smooth / 10.f, 75.0f / 100.0f);
								auto local_loc = viewMatrix.GetCameraVec();

								if (Target != Vector3(0, 0, 0)) {
									SDK->WPM<Vector3>(SDK->g_player_controller + 0x12A0, Target);
									if (in_range(local_angle, vec_calc_target, local_loc, vec, settings::aimbot::hitbox)) {
										//SDK->WPM<float>(GetSenstivePTR(), 0);
										if (settings::aimbot::hanzo_flick) {
											SetKeyHold(0x1000, 100);
										}
										else {
											SetKey(0x1);
										}
										//SDK->WPM<float>(GetSenstivePTR(), origin_sens);
										shooted = true;
									}
								}
							}
						}
						Sleep(1);
					}
				}

				//triggerbot 
				 if (!isHanzo && settings::aimbot::triggerbot)
				{
					 Vector3 world = GetVector3(false);
					 XMFLOAT3 min{ -0.1f, -0.05f, -0.1f }, max{ 0.1f, 0.17f, 0.1f };
					 XMFLOAT3 min2{ -0.16f, -0.3f, -0.16f }, max2{ 0.16f, 0.23f, 0.16f };
					 XMFLOAT3 XMEnPos = XMFLOAT3(world.X, world.Y, world.Z);

					 if (viewMatrix.IntersectRayWithAABB(viewMatrixTo, viewMatrixTo.GetCameraVec(), MyXMAngle, GetAsyncKeyState(VK_XBUTTON2) ? min : min2, GetAsyncKeyState(VK_XBUTTON2) ? max : max2, settings::aimbot::trigger_hitbox / 100.f, XMEnPos, GetAsyncKeyState(VK_XBUTTON2)))
					 {
						 Sleep(settings::aimbot::trigger_delay);
						 SetKey(0x1);
					 }
				}
				
				Sleep(1);
			}
		}
		__except (1) {

		}
	}
}