#pragma once

#include "includes.hpp"
typedef __int64(__fastcall* PInitRemoteKeyDetectionFunc)(VMM_HANDLE hVMM);

typedef __int64(__fastcall* Pis_key_down)(int key);

//typedef struct  HID
//{
//    DWORD dwWinlogonPid;
//    DWORD64 pWin32kbaseDriverAddress;
//    DWORD64 gafAsyncKeyStateAddress;
//    DWORD64 gafAsyncKeyStatePtr;
//    UINT8 KeyStateBitmap[256 * 2 / 8];
//    UINT8 KeyStateRecentBitmap[256 / 8];
//}HIDVariables;
//static HIDVariables HIDVars;

//class Key
//{
//private:
//    DWORD dwWinlogonPid;
//    DWORD64 pWin32kbaseDriverAddress;
//    DWORD64 gafAsyncKeyStateAddress;
//    DWORD64 gafAsyncKeyStatePtr;
//    UINT8 KeyStateBitmap[256 * 2 / 8];
//    UINT8 KeyStateRecentBitmap[256 / 8];
//    VMM_HANDLE _hVmm;
//public:
//    BOOL Init(VMM_HANDLE h)
//    {
//        this->_hVmm = h;
//        ULONG64 ulPIDCount = 0;
//        BOOL bProcessFound = FALSE;
//
//        if (VMMDLL_PidList(h, NULL, &ulPIDCount))
//        {
//            std::vector<DWORD> pdwPIDs = Memory::GetProcessPidList();
//            if (!pdwPIDs.empty())
//            {
//                for (auto dwProcessId : pdwPIDs)
//                {
//                    auto name = Memory::GetPidName(dwProcessId);
//                    if (strstr(name.c_str(), "winlogon.exe")) {
//                        bProcessFound = TRUE;
//                        this->dwWinlogonPid = dwProcessId;
//                        break;
//                    }
//                }
//
//                BOOL bFunctionFound = FALSE;
//                if (bProcessFound)
//                {
//                    PVMMDLL_MAP_MODULEENTRY pMapModuleEntry;
//                    // ZeroMemory(&pMapModuleEntry, sizeof(VMMDLL_MAP_MODULEENTRY));
//                    if (VMMDLL_Map_GetModuleFromNameU(h, this->dwWinlogonPid | VMMDLL_PID_PROCESS_WITH_KERNELMEMORY, (LPSTR)"win32kbase.sys", &pMapModuleEntry, NULL))
//                    {
//
//                        this->pWin32kbaseDriverAddress = pMapModuleEntry->vaBase;
//
//                        //printf("\n pWin32kbaseDriverAddress %d ", HIDVars.pWin32kbaseDriverAddress);
//
//                        PVMMDLL_MAP_EAT pEatMap = NULL;
//                        PVMMDLL_MAP_EATENTRY pEatMapEntry;
//                        if (VMMDLL_Map_GetEATU(h, this->dwWinlogonPid | VMMDLL_PID_PROCESS_WITH_KERNELMEMORY, (LPSTR)"win32kbase.sys", &pEatMap))
//                        {
//                            if (pEatMap->dwVersion == VMMDLL_MAP_EAT_VERSION)
//                            {
//                                for (int i = 0; i < pEatMap->cMap; i++)
//                                {
//                                    pEatMapEntry = pEatMap->pMap + i;
//                                    if (strcmp(pEatMapEntry->uszFunction, "gafAsyncKeyState") == 0)
//                                    {
//                                        bFunctionFound = TRUE;
//                                        this->gafAsyncKeyStateAddress = pEatMapEntry->vaFunction;
//                                        break;
//                                    }
//                                }
//                            }
//                        }
//                    }
//                    if (!bFunctionFound)
//                    {
//                        this->gafAsyncKeyStateAddress = this->pWin32kbaseDriverAddress + this->gafAsyncKeyStatePtr;
//                    }
//                    UINT8 bKeyArrayTest[64] = { 0 };
//                    if (VMMDLL_MemReadEx(h, this->dwWinlogonPid | VMMDLL_PID_PROCESS_WITH_KERNELMEMORY, this->gafAsyncKeyStateAddress, (PBYTE)&bKeyArrayTest, 64, NULL, VMMDLL_FLAG_NOCACHE))
//                    {
//                        for (int i = 0; i < sizeof(bKeyArrayTest) / sizeof(bKeyArrayTest[0]); i++)
//                        {
//                            if (bKeyArrayTest[i] != 0)
//                            {
//                                return TRUE;
//                            }
//                        }
//                    }
//                }
//
//
//            }
//        }
//        return FALSE;
//    }
//
//    void UpdateKeyInputBitmap(VMM_HANDLE h)
//    {
//        //printf("\n gafAsyncKeyStateAddress %d ", HIDVars.gafAsyncKeyStateAddress);
//        UINT8 prevKeyStateBitmap[64] = { 0 };
//        memcpy(prevKeyStateBitmap, this->KeyStateBitmap, 64);
//        if (VMMDLL_MemReadEx(h, this->dwWinlogonPid | VMMDLL_PID_PROCESS_WITH_KERNELMEMORY, this->gafAsyncKeyStateAddress, (PBYTE)&this->KeyStateBitmap, 64, NULL, VMMDLL_FLAG_NOCACHE))
//        {
//            for (int vk = 0; vk < 256; ++vk)
//            {
//                if ((this->KeyStateBitmap[(vk * 2 / 8)] & 1 << vk % 4 * 2) && !(prevKeyStateBitmap[(vk * 2 / 8)] & 1 << vk % 4 * 2))
//                {
//                    this->KeyStateRecentBitmap[vk / 8] |= 1 << vk % 8;
//                }
//            }
//        }
//    }
//
//    BOOL gafAsyncKeyState(VMM_HANDLE h, UINT32 vKey)
//    {
//        UpdateKeyInputBitmap(h);
//        return this->KeyStateBitmap[(vKey * 2 / 8)] & 1 << vKey % 4 * 2;
//    }
//
//    inline __int64 IsPress(int key)
//    {
//        // Pis_key_down is_key_down = reinterpret_cast<Pis_key_down>(GetProcAddress(Module, "is_key_down"));
//        // return is_key_down(key);
//        // return GetAsyncKeyState(key);
//        return this->gafAsyncKeyState(this->_hVmm, key);
//        // return false;
//    }
//
//};


class Key
{
public:
	inline __int64 Init(VMM_HANDLE hVMM)
	{
		HMODULE GModule = LoadLibrary("vmm.dll");
		//printf("Module %p\n", GModule);
		this->Module = GModule;
		PInitRemoteKeyDetectionFunc InitRemoteKeyDetectionFunc = (PInitRemoteKeyDetectionFunc)GetProcAddress(Module, "InitRemoteKeyDetection");
		//printf("InitRemoteKeyDetectionFunc %p\n", InitRemoteKeyDetectionFunc);
		this->hVMM = hVMM;
		return InitRemoteKeyDetectionFunc(hVMM);
	};
	inline __int64 IsKeyDown(int key)
	{
		Pis_key_down is_key_down = reinterpret_cast<Pis_key_down>(GetProcAddress(Module, "is_key_down"));
		return is_key_down(key);
	};

	inline __int64 IsClick(int key)
	{
		Pis_key_down is_key_down = reinterpret_cast<Pis_key_down>(GetProcAddress(Module, "was_key_pressed"));
		return is_key_down(key);
	};
public:
	HMODULE Module;
	VMM_HANDLE hVMM;
};

namespace K { inline Key vmm; };
