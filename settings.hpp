#pragma once

namespace OW {
	namespace settings {
		namespace defines {
			bool menu = true;
			int menukey = VK_INSERT;
		}

		namespace aimbot {
			bool aimbot = true;
			float aimbot_smooth = 0.5f;

			bool flick = true;
			float flick_smooth = 0.5f;

			bool hanzo_flick;

			bool prediction;
			float prediction_level = 110.f;

			bool triggerbot;
			bool triggerbot_usehotkey;
			float trigger_hitbox = 175.0f;
			float trigger_delay = 50.0f;

			int hero = 0;
			const char* heroes[] = { "Hanzo", "Genji", "Ana", "Sojourn", "Lucio", "Eco", "Pharah", "Orisa", "Torb", "Zen" };

			float fov = 200.f;
			float hitbox = 0.130f;

			int aimkey = VK_LBUTTON;
			int flickkey = VK_XBUTTON2;
		}

		namespace wallhack {
			bool fov = true;
			bool box;
			bool health;
			bool distance;
			bool heroname;
			bool only_visible;
			bool visible_check = true;
			bool outline_players = true;
			int box_type = 1;
			bool on_hotkey;
			bool toggle;
			bool toggled;
			int hotkey = 0x49;

			bool test = true;
		}

		namespace misc {
			bool auto_heal;
			float min_health = 75.0f;
			bool auto_melee;
			int meleekey = 0x56;
			float min_dist = 2.0f;
		}

		namespace color {
			ImColor visible_color(0, 255, 84, 255);
			ImColor invisible_color(255, 0, 84, 255);
			ImColor team_color(255, 255, 84, 255);
		}
	}
}