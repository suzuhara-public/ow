#pragma once
#include "Vector.hpp"

namespace OW {
	namespace draw {
		void Text(const ImVec2& pos, ImColor color, const char* str, float size = 15.f)
		{
			ImGui::GetBackgroundDrawList()->AddText(NULL, size, ImVec2(pos.x, pos.y), color, str);
		}

		void DrawLine(const Vector2& from, const Vector2& to, ImColor color, float thickness = 1.0f)
		{
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(from.X, from.Y), ImVec2(to.X, to.Y), color, thickness);
		}

		void Draw2DBox(float x, float y, float w, float h, ImColor color, int thickness = 2.0f)
		{
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(x, y), ImVec2(x + w, y), color, thickness); // top 
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(x, y), ImVec2(x, y + h), color, thickness); // left
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(x + w, y), ImVec2(x + w, y + h), color, thickness);  // right
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(x, y + h), ImVec2(x + w, y + h), color, thickness);   // bottom 
		}

		void DrawCorneredBox(int X, int Y, int W, int H, ImColor color, int thickness = 2.0f)
		{
			float lineW = (W / 5);
			float lineH = (H / 5);

			//corners
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X, Y), ImVec2(X, Y + lineH), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X, Y), ImVec2(X + lineW, Y), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X + W - lineW, Y), ImVec2(X + W, Y), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X + W, Y), ImVec2(X + W, Y + lineH), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X, Y + H - lineH), ImVec2(X, Y + H), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X, Y + H), ImVec2(X + lineW, Y + H), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X + W - lineW, Y + H), ImVec2(X + W, Y + H), color, thickness);
			ImGui::GetBackgroundDrawList()->AddLine(ImVec2(X + W, Y + H - lineH), ImVec2(X + W, Y + H), color, thickness);
		}

		void DrawInfo(const ImVec2& Pos, int HeightSize, std::string Info, float distance, float EnHealth, float EnHealthMax) {
			ImVec2 InfoSize = ImGui::CalcTextSize(Info.c_str());
			if (distance < 200)
			{
				if (EnHealth > 0.f)
				{
					ImGui::GetBackgroundDrawList()->AddRectFilled(ImVec2(Pos.x - (InfoSize.x / 2), Pos.y + (HeightSize / 2)), ImVec2(Pos.x + (InfoSize.x / 2) + 35, Pos.y - (HeightSize / 2)), ImGui::GetColorU32(ImVec4(0, 0, 0, 0.7)), 2.5f);
					//ImGui::GetBackgroundDrawList()->AddRectFilled(ImVec2(Pos.x - (InfoSize.x / 2) + 7, Pos.y + (HeightSize / 2) - 6), ImVec2(Pos.x - (InfoSize.x / 2) + 7 + ((abs((Pos.x - (InfoSize.x / 2) + 7) - (Pos.x + (InfoSize.x / 2))) / EnHealthMax) * EnHealth), Pos.y + (HeightSize / 2) - 2), ImGui::GetColorU32(ImVec4(0, 1, 0, 1)));
					Text(ImVec2(Pos.x - ((InfoSize.x / 2)) + 10, Pos.y - (HeightSize / 2)), ImColor(255,255,255,255), Info.c_str());
				}
				else
				{
					ImGui::GetBackgroundDrawList()->AddRectFilled(ImVec2(Pos.x - (InfoSize.x / 2), Pos.y + (HeightSize / 2)), ImVec2(Pos.x + (InfoSize.x / 2) + 35, Pos.y - (HeightSize / 2)), ImGui::GetColorU32(ImVec4(0, 0, 0, 0.7)));
					//ImGui::GetBackgroundDrawList()->AddRectFilled(ImVec2(Pos.x - (InfoSize.x / 2) + 7, Pos.y + (HeightSize / 2) - 6), ImVec2(Pos.x - (InfoSize.x / 2) + 7 + ((abs((Pos.x - (InfoSize.x / 2) + 7) - (Pos.x + (InfoSize.x / 2))) / EnHealthMax) * EnHealth), Pos.y + (HeightSize / 2) - 2), ImGui::GetColorU32(ImVec4(0, 1, 0, 1)));
					Text(ImVec2(Pos.x - ((InfoSize.x / 2)) + 10, Pos.y - (HeightSize / 2)), ImColor(255, 255, 255, 255), Info.c_str());
				}
			}
		}

		void DrawBar(const ImVec2& Pos, int HeightSize, std::string Info)
		{
			ImVec2 InfoSize = ImVec2(50, 90);
			ImGui::GetBackgroundDrawList()->AddRectFilled(ImVec2(Pos.x - (InfoSize.x / 2), Pos.y + (HeightSize / 2)), ImVec2(Pos.x + (InfoSize.x / 2) + 35, Pos.y - (HeightSize / 2)), ImColor(0, 0, 0, 15));
		}

		void DrawQuadFilled(const ImVec2& pos1, const ImVec2& pos2, const ImVec2& pos3, const ImVec2& pos4, ImColor color, float thickness, float rounding)
		{
			ImGui::GetBackgroundDrawList()->AddQuadFilled(pos1, pos2, pos3, pos4, color);
		}

		void DrawHealthBar(int width, const ImVec2& from, int currentHealth, int maxHealth, int currentArmor, int maxArmor, int currentBarrier, int maxBarrier)
		{
			int height = width * 0.1;
			int indent = width * 0.02;
			int blockSize = width * 0.06;
			int space = width * 0.01;
			int nbBlock = width / blockSize;
			int nbBlockArmor = 0;

			if (maxArmor != 0 && currentArmor != 0) { //maxArmor != 0 &&
				int nbBlockArmorMax = nbBlock * 0.25;
				nbBlockArmor = (currentArmor * nbBlockArmorMax) / maxArmor;
			}
			int nbBlockBarrier = 0;
			if (maxBarrier != 0 && currentBarrier != 0) { //maxBarrier != 0 &&
				int nbBlockBarrierMax = nbBlock * 0.25;
				nbBlockBarrier = (currentBarrier * nbBlockBarrierMax) / maxBarrier;
			}
			int nbBlockHealthFull = (currentHealth * (nbBlock - nbBlockArmor - nbBlockBarrier)) / maxHealth;

			for (int i = 0; i < nbBlock; i++) {
				int pos1X = from.x + (i * (blockSize + space));
				int pos1Y = from.y;
				ImVec2 pos1(pos1X, pos1Y);

				int pos2X = from.x + blockSize + (i * (blockSize + space));
				int pos2Y = from.y;
				ImVec2 pos2(pos2X, pos2Y);

				int pos3X = from.x + indent + blockSize + (i * (blockSize + space));
				int pos3Y = from.y - height;
				ImVec2 pos3(pos3X, pos3Y);

				int pos4X = from.x + indent + (i * (blockSize + space));
				int pos4Y = from.y - height;
				ImVec2 pos4(pos4X, pos4Y);

				if (i < nbBlockHealthFull) {
					DrawQuadFilled(pos4, pos3, pos2, pos1, ImColor(255, 0, 0, 255), 0, 0);
				}
				else {
					if (nbBlockArmor > 0 && i < (nbBlockHealthFull + nbBlockArmor)) {
						DrawQuadFilled(pos4, pos3, pos2, pos1, ImColor(255, 220, 49, 255), 0, 0);
					}
					else {
						if (nbBlockBarrier > 0 && i < (nbBlockHealthFull + nbBlockArmor + nbBlockBarrier)) {
							DrawQuadFilled(pos4, pos3, pos2, pos1, ImColor(114, 189, 234, 255), 0, 0);
						}
						else {
							DrawQuadFilled(pos4, pos3, pos2, pos1, ImColor(80, 80, 80, 125), 0, 0);
						}
					}
				}
			}
		}
	}
}