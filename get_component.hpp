#pragma once
#include "idadefs.hpp"
namespace OW {

	inline std::string GetHeroNames(uint64_t HeroID, uint64_t LinkBase) {
		switch (HeroID)
		{
		case eHero::HERO_REAPER:
			return skCrypt("Reaper").decrypt();
		case eHero::HERO_TRACER:
			return skCrypt("Tracer").decrypt();
		case eHero::HERO_MERCY:
			return skCrypt("Mercy").decrypt();
		case eHero::HERO_HANZO:
			return skCrypt("Hanzo").decrypt();
		case eHero::HERO_TORBJORN:
			return skCrypt("Torbjorn").decrypt();
		case eHero::HERO_REINHARDT:
			return skCrypt("Reinhardt").decrypt();
		case eHero::HERO_PHARAH:
			return skCrypt("Pharah").decrypt();
		case eHero::HERO_WINSTON:
			return skCrypt("Winston").decrypt();
		case eHero::HERO_WIDOWMAKER:
			return skCrypt("Widowmaker").decrypt();
		case eHero::HERO_BASTION:
			return skCrypt("Bastion").decrypt();
		case eHero::HERO_SYMMETRA:
			return skCrypt("Symmetra").decrypt();
		case eHero::HERO_ZENYATTA:
			return skCrypt("Zenyatta").decrypt();
		case eHero::HERO_GENJI:
			return skCrypt("Genji").decrypt();
		case eHero::HERO_ROADHOG:
			return skCrypt("Roadhog").decrypt();
		case eHero::HERO_MCCREE:
			return skCrypt("McCree").decrypt();
		case eHero::HERO_JUNKRAT:
			return skCrypt("Junkrat").decrypt();
		case eHero::HERO_ZARYA:
			return skCrypt("Zarya").decrypt();
		case eHero::HERO_SOLDIER76:
			return skCrypt("Soldier 76").decrypt();
		case eHero::HERO_LUCIO:
			return skCrypt("Lucio").decrypt();
		case eHero::HERO_DVA:
			if (SDK->RPM<uint16_t>(LinkBase + 0xD4) != SDK->RPM<uint16_t>(LinkBase + 0xD8))
				return skCrypt("D.Va").decrypt();
			else
				return skCrypt("Hana").decrypt();
		case eHero::HERO_MEI:
			return skCrypt("Mei").decrypt();
		case eHero::HERO_ANA:
			return skCrypt("Ana").decrypt();
		case eHero::HERO_SOMBRA:
			return skCrypt("Sombra").decrypt();
		case eHero::HERO_ORISA:
			return skCrypt("Orisa").decrypt();
		case eHero::HERO_DOOMFIST:
			return skCrypt("Doomfist").decrypt();
		case eHero::HERO_MOIRA:
			return skCrypt("Moira").decrypt();
		case eHero::HERO_BRIGITTE:
			return skCrypt("Brigitte").decrypt();
		case eHero::HERO_WRECKINGBALL:
			return skCrypt("Wrecking Ball").decrypt();
		case eHero::HERO_SOJOURN:
			return skCrypt("Sojourn").decrypt();
		case eHero::HERO_ASHE:
			return skCrypt("Ashe").decrypt();
		case eHero::HERO_BAPTISTE:
			return skCrypt("Baptiste").decrypt();
		case eHero::HERO_KIRIKO:
			return skCrypt("Kiriko").decrypt();
		case eHero::HERO_JUNKERQUEEN:
			return skCrypt("Junker Queen").decrypt();
		case eHero::HERO_SIGMA:
			return skCrypt("Sigma").decrypt();
		case eHero::HERO_ECHO:
			return skCrypt("Echo").decrypt();
		case eHero::HERO_RAMATTRA:
			return skCrypt("Ramattra").decrypt();
		case eHero::HERO_TRAININGBOT1:
			return skCrypt("Bot").decrypt();
		case eHero::HERO_TRAININGBOT2:
			return skCrypt("Bot").decrypt();
		case eHero::HERO_TRAININGBOT3:
			return skCrypt("Bot").decrypt();
		case eHero::HERO_TRAININGBOT4:
			return skCrypt("Bot").decrypt();
		default:
			return skCrypt("Unknown").decrypt();
		}
	}

	// visibility comp decrypt
	unsigned __int64 __fastcall DecryptVis(__int64 a1) {
		__int64 v1; // rsi
		unsigned __int64 v2; // rbx
		unsigned __int64 v3; // r9
		__int64 v4; // rsi
		unsigned __int64 v5; // rdx
		unsigned __int64 v6; // rcx
		__m128i v7; // xmm1
		__m128i v8; // xmm2
		__m128i v9; // xmm0
		__m128i v10; // xmm1
		v1 = a1;
		v2 = SDK->dwGameBase + offset::Visible_Fn;
		v3 = v2 + 0x8;
		v4 = SDK->RPM<uint64_t>(SDK->dwGameBase + 0x37BBDC0
			+ 8 * (((uint8_t)a1 - 0x71) & 0x7F)
			+ (((uint64_t)(a1 - offset::Visible_Key) >> 7) & 7)) ^ v2 ^ (a1 - offset::Visible_Key);
		v5 = (v3 - v2 + 7) >> 3;
		v6 = 0i64;
		if (v2 > v3)
			v5 = 0i64;
		if (v5) {
			if (v5 >= 4) {
				ZeroMemory(&v7, sizeof(v7));
				ZeroMemory(&v8, sizeof(v8));
				do {
					v6 += 4i64;
					v7 = _mm_xor_si128(v7, _mm_loadu_si128((const __m128i*)v2));
					v9 = _mm_loadu_si128((const __m128i*)(v2 + 16));
					v2 += 32i64;
					v8 = _mm_xor_si128(v8, v9);
				} while (v6 < (v5 & 0xFFFFFFFFFFFFFFFCui64));
				v10 = _mm_xor_si128(v7, v8);
				auto addr = _mm_xor_si128(v10, _mm_srli_si128(v10, 8));
				v4 ^= *(__int64*)&addr;
			}
		}
		for (; v6 < v5; ++v6) {
			v4 ^= SDK->RPM<uintptr_t>(v2);
			v2 += 8i64;
		}
		return v4 ^ ~v3 ^ 0xD372C9F043A5E88F;
	}

	uint64_t GetOutlineStruct(uint64_t a1) //40 53 48 83 EC 30 65 48 8B 04 25 ? ? ? ? 48
	{
		__try
		{
			uint64_t result = NULL;
			int v2 = SDK->RPM<int>(a1 + 0x68);
			if (v2 <= 0)
				result = 0;
			else
				result = (uint64_t)(0x20 * v2 + SDK->RPM<_QWORD>(a1 + 0x60) - 0x20i64);

			return result;
		}
		__except (EXCEPTION_EXECUTE_HANDLER) {}

		return NULL;
	}

	uint64_t __fastcall GetColorStruct(__int64 a1) //40 57 48 83 EC 20 44 8B 91
	{
		__try
		{
			uint64_t result = NULL;
			int v2 = SDK->RPM<int>(a1 + 0xF8);
			if (v2 <= 0)
				result = 0;
			else
				result = (SDK->RPM<_QWORD>(a1 + 0xF0) + 0x14 * v2 - 0x14);

			return result;
		}
		__except (EXCEPTION_EXECUTE_HANDLER) {}
		return NULL;
	}
	uint64_t __fastcall DecryptOutline(__int64 a1)
	{
		__int64 v1; // rbx
		unsigned __int64 v2; // rdi
		unsigned __int64 v3; // rax
		__int64 v4; // rbx
		unsigned __int64 v5; // rdx
		unsigned __int64 v6; // rcx
		__m128i v7; // xmm1
		__m128i v8; // xmm2
		__m128i v9; // xmm0
		__m128i v10; // xmm1

		v2 = SDK->dwGameBase + offset::Outline_Fn;
		v3 = v2 + 0x8;

		uint64_t keys = SDK->dwGameBase + 0x37BBDC0 + 8 * (((_BYTE)a1 - 0x53184608) & 0x7F);
		v4 = v2 ^ SDK->RPM<DWORD_PTR>(keys
			+ (((unsigned __int64)(a1 - offset::Outline_Key) >> 7) & 7)) ^ (a1 - offset::Outline_Key);

		v5 = (v3 - v2 + 7) >> 3;
		v6 = 0i64;
		if (v2 > v3)
			v5 = 0i64;
		if (v5)
		{
			if (v5 >= 4)
			{
				v7 = {};
				v8 = {};
				do
				{
					v6 += 4i64;
					v7 = _mm_xor_si128(v7, _mm_loadu_si128((const __m128i*)v2));
					v9 = _mm_loadu_si128((const __m128i*)(v2 + 16));
					v2 += 0x20i64;
					v8 = _mm_xor_si128(v8, v9);
				} while (v6 < (v5 & 0xFFFFFFFFFFFFFFFCui64));
				v10 = _mm_xor_si128(v7, v8);
				__m128i P1 = _mm_xor_si128(v10, _mm_srli_si128(v10, 8));
				v4 ^= *(DWORD_PTR*)&P1;
			}
			for (; v6 < v5; ++v6)
			{
				v4 ^= SDK->RPM<DWORD_PTR>(v2);
				v2 += 8i64;
			}
		}
		return v4 ^ ~v3 ^ 0x0A085A5C2ACE7B9F8;
	}

	//48 89 5c 24 ? 48 89 6c 24 ? 56 57 41 56 48 83 ec ? 49 8b f1 49 8b e8
	uintptr_t DecryptComponent(uintptr_t parent, uint8_t idx) {
		__try {
			if (parent) {
				unsigned __int64 v1 = parent;
				unsigned __int64 v2 = (uintptr_t)1 << (uintptr_t)(idx & 0x3F);
				unsigned __int64 v3 = v2 - 1;
				unsigned __int64 v4 = idx & 0x3F;
				unsigned __int64 v5 = idx / 0x3F;
				unsigned __int64 v6 = SDK->RPM<uintptr_t>((v1 + 8 * (uint32_t)v5 + 0x110));
				unsigned __int64 v7 = (v2 & SDK->RPM<uintptr_t>((v1 + 8 * (uint32_t)v5 + 0x110))) >> v4;
				unsigned __int64 v8 = (v3 & v6) - (((v3 & v6) >> 1) & 0x5555555555555555);
				unsigned __int64 v9 = SDK->RPM<uintptr_t>((SDK->RPM<uintptr_t>((v1 + 0x80)) + 8 * (SDK->RPM<uint8_t>(((uint32_t)v5 + v1 + 0x130)) + ((0x101010101010101 * (((v8 & 0x3333333333333333) + ((v8 >> 2) & 0x3333333333333333) + (((v8 & 0x3333333333333333) + ((v8 >> 2) & 0x3333333333333333)) >> 4)) & 0xF0F0F0F0F0F0F0F)) >> 0x38))));
				unsigned __int64 Key1 = SDK->GlobalKey1;
				unsigned __int64 Key2 = SDK->GlobalKey2;
				unsigned __int64 dummy = SDK->RPM<_QWORD>(SDK->dwGameBase + 0x37BADA0 + (Key1 >> 0x34));
				unsigned __int64 dummy2 = SDK->RPM<_QWORD>(SDK->dwGameBase + 0x37BADA0 + (Key1 & 0xFFF));

				unsigned __int64 v10 = (unsigned int)v9 | v9 & 0xFFFFFFFF00000000ui64 ^ (((unsigned int)v9 ^ 0xFFFFFFFFD1063558ui64) << 0x20);
				unsigned __int64 v11 = (unsigned int)Key2 ^ (unsigned int)v10 | (Key2 ^ ((unsigned int)v10 | v10 & 0xFFFFFFFF00000000ui64 ^ (((unsigned int)v10 ^ HIDWORD(dummy)) << 0x20))) & 0xFFFFFFFF00000000ui64 ^ ((unsigned __int64)(2 * __ROR4__(dummy2, 3) - ((unsigned int)Key2 ^ (unsigned int)v10)) << 0x20);
				unsigned __int64 v12 = -(int)v7 & ((unsigned int)v11 | v11 & 0xFFFFFFFF00000000ui64 ^ ((unsigned __int64)(unsigned int)(v11 + 0x3ABE0EE6) << 0x20));
				return v12;
			}
		}
		__except (EXCEPTION_EXECUTE_HANDLER) {}
		return NULL;
	}

	inline uint64_t DecryptParent(uint64_t ComponentBase)
	{
		uint64_t qword_37BAE87 = SDK->RPM<uint64_t>(SDK->dwGameBase + 0x37BAE87);
		uint64_t key = qword_37BAE87 ^ 0xEFE7068153535FE2;
		uint64_t ParentEncrypt = SDK->RPM<uint64_t>(ComponentBase + 0x8);
		return key ^ ParentEncrypt;
	}

	inline uintptr_t GetGManagerByComponent(uintptr_t ComponentBase) {
		auto Parent = DecryptParent(ComponentBase);
		auto v4 = SDK->RPM<_QWORD>(Parent + 0x28);
		return v4 ^ 0x8F7A0E8A42B59A5D;
	}

	inline bool ParentContainsComponent(uintptr_t parent, uint8_t idx) {
		__int64 v2 = SDK->RPM<_QWORD>(parent + 8 * ((unsigned __int64)idx >> 6) + 0x110);
		return _bittest64(&v2, idx & 0x3F);
	}

	inline uintptr_t GetEntityTArray(uintptr_t GManager) {
		if (GManager) {
			uint64_t xor_key = SDK->RPM<uint64_t>(SDK->dwGameBase + 0x37BBAE4) ^ 0xFE18383FF40278BA;
			return SDK->RPM<uint64_t>(GManager + 0x18) ^ xor_key;
		}
		return 0;
	}

	static uint64_t GetEntityWithUID(uint64_t pGManager, uint32_t UID)
	{
		static struct TArray
		{
			uint64_t base_address;
			uint32_t num_elements;
		};

		__try
		{
			uint64_t result = 0;

			uint64_t pEntityTArray = GetEntityTArray(pGManager);
			TArray CurrTArray = SDK->RPM<TArray>(pEntityTArray + 0x10 * (UID & 0xFFF));

			auto hScatter = SDK->hScatterEntityList;
			Memory::ScatterClear(SDK->dwPID, hScatter, 1);

			if (CurrTArray.base_address && CurrTArray.num_elements && CurrTArray.num_elements < 100)
			{
				for (int i = 0; i < CurrTArray.num_elements; i++)
				{
					Memory::ScatterPrepare(hScatter, CurrTArray.base_address + 0x10 * i, sizeof(uint32_t));
					Memory::ScatterPrepare(hScatter, CurrTArray.base_address + 0x10 * i + 8, sizeof(uint64_t));
				}

				Memory::ExecuteRead(hScatter);

				for (int i = 0; i < CurrTArray.num_elements; i++)
				{
					uint32_t CurrUID = SDK->ScatterRPM<uint32_t>(CurrTArray.base_address + 0x10 * i, hScatter);
					uint64_t CurrEntity = SDK->ScatterRPM<uint64_t>(CurrTArray.base_address + 0x10 * i + 8, hScatter);

					if (SDK->RPM<uint32_t>(CurrEntity + 0x138, false) == CurrUID && CurrUID == UID)
					{
						result = CurrEntity;
					}
				}
			}

			return result;
		}
		__except (EXCEPTION_EXECUTE_HANDLER)
		{
			return NULL;
		}
	}

	static time_t last_update = 0;
	static uint64_t GetCommonWithReaper(uint64_t Reaper)
	{
		uint64_t CommonLinker = DecryptComponent(Reaper, TYPE_LINK);
		time_t tick = GetTickCount64();
		if (!SDK->GameManager || tick - last_update > 1000 * 5) {
			SDK->GameManager = GetGManagerByComponent(CommonLinker);
			last_update = tick;
		}
		return GetEntityWithUID(SDK->GameManager, SDK->RPM<uint64_t>(CommonLinker + 0xD4));
	}

	__forceinline std::vector<std::pair<uint64_t, uint64_t>> get_ow_entities() {
		std::vector<std::pair<uintptr_t, uintptr_t>> result{};
		uintptr_t entity_list = SDK->RPM<uintptr_t>(SDK->dwGameBase + offset::Address_entity_list);
		MY_MEMORY_BASIC_INFORMATION64 mbi = SDK->MyVirtualQueryOne(entity_list);
		size_t entity_list_size = mbi.vaEnd - mbi.vaStart;

		for (size_t i = 0; i < entity_list_size; i += 0x10) {
			uintptr_t cur_entity = SDK->RPM<uintptr_t>(entity_list + i);
			if (!cur_entity)
				continue;

			if (!ParentContainsComponent(cur_entity, TYPE_LINK)) continue;
			
			uintptr_t common = GetCommonWithReaper(cur_entity);
			if (!common)
				continue;
			result.emplace_back(common, cur_entity);
		}
		return result;
	}
}