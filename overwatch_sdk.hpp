#pragma once
#include "memory/dma/Xemory.h"
#include "key.hpp"
#include "includes.hpp"

typedef struct MY_MEMORY_BASIC_INFORMATION64
{
	QWORD vaStart;
	QWORD vaEnd;
	DWORD Protection;
	DWORD fPrivateMemory;
	DWORD MemCommit;
};

namespace OW {
	class MemorySDK {
	private:
		std::vector<MEMORY_BASIC_INFORMATION64> mbis;
		bool CompressMbis = false;
	public:
		HANDLE hProcess = 0;
		DWORD dwPID = 0;
		uint64_t dwGameBase = 0;
		uint64_t GlobalKey1 = 0, GlobalKey2 = 0;
		uint64_t g_player_controller = 0;
		VMMDLL_SCATTER_HANDLE hScatterVM;
		VMMDLL_SCATTER_HANDLE hScatterProp;
		VMMDLL_SCATTER_HANDLE hScatterEntityList;
		uint64_t GameManager = 0;
	public:
		inline bool Initialize()
		{
			Memory::Initialize();
			dwPID = Memory::GetProcessPid("Overwatch.exe");
			dwGameBase = Memory::GetModuleFromName(dwPID);
			hScatterVM = Memory::ScatterInitialize(dwPID, 3);
			hScatterProp = Memory::ScatterInitialize(dwPID, 3);
			hScatterEntityList = Memory::ScatterInitialize(dwPID, 3);
			return dwGameBase;
		}

		inline bool GetGlobalKey() {
			GlobalKey1 = 0x5baee68c9deb6cdf;
			GlobalKey2 = 0xb92e0edea86ab11c;
			return true;
		}

		inline bool GetGlobalKey1() {
			static auto key_sig = (BYTE*)"\x00\x00\x00\x00\x80\x00\x00\x40\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x3f";
			static auto key_mask = "x???xx?xx?????xxxxxx";
			while (true) {
				// uint64_t Key = FindPatternExReg(key_sig, key_mask, 0x100000) - 0x70;
				uint64_t Key = MyFindPatternExReg(key_sig, key_mask, 0x100000) - 0x70;
				if (Key && RPM<uint64_t>(Key + 0x18) > 0x10000000000000 && RPM<uint64_t>(Key + 0x28) > 0x10000000000000) {
					GlobalKey1 = RPM<uint64_t>(Key + 0x18);
					GlobalKey2 = RPM<uint64_t>(Key + 0x20);
					std::cout << std::hex << "key1 = 0x" << GlobalKey1 << ", key2 = 0x" << GlobalKey2 << std::endl;
					return true;
				}
				Sleep(1000);
			}
		}

		template <typename WPMType>
		__forceinline bool WPM(DWORD_PTR address, WPMType buffer)
		{
			return Memory::WriteMemory(dwPID, address, &buffer, sizeof(buffer));
		}

		template <typename RPMType>
		__forceinline RPMType RPM(DWORD_PTR address, bool noCache = true)
		{
			RPMType buffer;
			Memory::ReadMemory(dwPID, address, &buffer, sizeof(RPMType));
			return buffer;
		}

		template <typename RPMType>
		__forceinline RPMType RPMEX(DWORD_PTR address, bool noCache = true)
		{
			RPMType buffer;
			Memory::ReadMemoryEx(dwPID, address, &buffer, sizeof(RPMType), noCache ? 1 : 0);
			return buffer;
		}

		template <typename RPMType>
		__forceinline RPMType ScatterRPM(DWORD_PTR address, VMMDLL_SCATTER_HANDLE h)
		{
			RPMType buffer;
			Memory::ScatterRead(h, address, &buffer, sizeof(RPMType));
			return buffer;
		}

		__forceinline uintptr_t calc_relative(uintptr_t current, int32_t relative)
		{
			return current + RPM<int32_t>(current) + relative;
		}
		__forceinline void read_buf(const DWORD_PTR address, char* buffer, SIZE_T size) {
			Memory::ReadMemory(dwPID, address, buffer, size);
		}

		template<typename T>
		bool ReadArray(uint64_t address, T out[], size_t len, bool noCache = true)
		{
			return Memory::ReadMemory(dwPID, address, out, len);
		}

		__forceinline bool UpdateMemoryQuery()
		{
			MEMORY_BASIC_INFORMATION64 mbi = { 0, };
			MEMORY_BASIC_INFORMATION64 old = { 0, };
			DWORD64 current_address = 0x7ffe0000;
			std::vector<MEMORY_BASIC_INFORMATION64> addresses;
			while (true)
			{
				if (!VirtualQueryEx(hProcess, (LPVOID)current_address, (PMEMORY_BASIC_INFORMATION)&mbi, sizeof(MEMORY_BASIC_INFORMATION64)))
					break;
				if ((mbi.State & 0x1000) != 0 && (mbi.Protect & 0x100) == 0)
				{
					if (old.BaseAddress + old.RegionSize == mbi.BaseAddress && CompressMbis)
						old.RegionSize += mbi.RegionSize;
					else
						addresses.push_back(mbi);

					old = mbi;
				}
				current_address = mbi.BaseAddress + mbi.RegionSize;
			}

			mbis = addresses;
			return (mbis.size() > 0);
		}

		__forceinline DWORD64 FindPattern(BYTE* buffer, BYTE* pattern, std::string mask, int bufSize)
		{
			int pattern_len = mask.length();
			for (int i = 0; i < bufSize - pattern_len; i++)
			{
				bool found = true;
				for (int j = 0; j < pattern_len; j++)
				{
					if (mask[j] != '?' && pattern[j] != buffer[(i + j)])
					{
						found = false;
						break;
					}
				}
				if (found)
					return i;
			}
			return -1;
		}

		__forceinline std::vector<DWORD64> FindPatternEx(DWORD64 start, DWORD64 end, BYTE* pattern, std::string mask, DWORD64 RgSize)
		{
			DWORD64 current_chunk = start;
			std::vector<DWORD64> found;
			if ((end - current_chunk > RgSize && RgSize != 0) || (end - current_chunk < RgSize && RgSize != 0))
				return found;
			while (current_chunk < end)
			{
				int bufSize = (int)(end - start);
				BYTE* buffer = new BYTE[bufSize];
				Memory::ReadMemory(dwPID, current_chunk, buffer, bufSize);
				if (!buffer)
				{
					current_chunk += bufSize;
					delete[] buffer;
					continue;
				}

				DWORD64 internal_address = FindPattern(buffer, pattern, mask, bufSize);
				if (internal_address != -1)
				{
					found.push_back(current_chunk + internal_address);
				}
				current_chunk += bufSize;
				delete[] buffer;

			}
			return found;
		}

		__forceinline std::vector<DWORD64> FindPatterns(BYTE* buffer, BYTE* pattern, std::string mask, int bufSize)
		{
			std::vector<DWORD64> ret;
			int pattern_len = mask.length();
			for (int i = 0; i < bufSize - pattern_len; i++)
			{
				bool found = true;
				for (int j = 0; j < pattern_len; j++)
				{
					if (mask[j] != '?' && pattern[j] != buffer[i + j])
					{
						found = false;
						break;
					}
				}
				if (found)
					ret.push_back(i);
			}
			return ret;
		}

		__forceinline DWORD64 FindPatternExReg(BYTE* pattern, std::string mask, DWORD64 RgSize)
		{
			if (!UpdateMemoryQuery())
				return 0;

			for (int i = 0; i < mbis.size(); i++) {
				MEMORY_BASIC_INFORMATION64 info = mbis[i];

				std::vector<DWORD64> arr = FindPatternEx(info.BaseAddress, info.RegionSize + info.BaseAddress, pattern, mask, RgSize);
				if (arr.size() > 0)
					return arr[0];
			}
			return 0;
		}

		MY_MEMORY_BASIC_INFORMATION64 MyVirtualQueryOne(DWORD64 lpAddress)
		{
			PVMMDLL_MAP_VAD pVadMap = NULL;
			PVMMDLL_MAP_VADENTRY pVadMapEntry;
			std::vector<MY_MEMORY_BASIC_INFORMATION64> resultset;
			BOOL result = VMMDLL_Map_GetVadU(Memory::_hVMM, dwPID, TRUE, &pVadMap);
			if (!result) {
				printf("FAIL:    VMMDLL_Map_GetVadU\n");
				return {};
			}
			if (pVadMap->dwVersion != VMMDLL_MAP_VAD_VERSION) {
				printf("FAIL:    VMMDLL_Map_GetVadU - BAD VERSION\n");
				VMMDLL_MemFree(pVadMap); pVadMap = NULL;
				return {};
			}
			for (DWORD i = 0; i < pVadMap->cMap; i++) {
				pVadMapEntry = &pVadMap->pMap[i];

				// 从lpAddress开始
				if (pVadMapEntry->vaStart <= lpAddress && pVadMapEntry->vaEnd >= lpAddress)
				{
					MY_MEMORY_BASIC_INFORMATION64 item = {
						pVadMapEntry->vaStart,
						pVadMapEntry->vaEnd + 1,
						pVadMapEntry->Protection,
						pVadMapEntry->fPrivateMemory,
						pVadMapEntry->MemCommit,
					};
					return item;
				}
			}
			VMMDLL_MemFree(pVadMap); pVadMap = NULL;
			return {};
		}

		std::vector<MY_MEMORY_BASIC_INFORMATION64> MyVirtualQueryEx(DWORD64 lpAddress)
		{
			CHAR szVadProtection[7] = { 0 };
			PVMMDLL_MAP_VAD pVadMap = NULL;
			PVMMDLL_MAP_VADENTRY pVadMapEntry;

			std::vector<MY_MEMORY_BASIC_INFORMATION64> resultset;

			BOOL result = VMMDLL_Map_GetVadU(Memory::_hVMM, dwPID, TRUE, &pVadMap);
			if (!result) {
				printf("FAIL:    VMMDLL_Map_GetVadU\n");
				return {};
			}
			if (pVadMap->dwVersion != VMMDLL_MAP_VAD_VERSION) {
				printf("FAIL:    VMMDLL_Map_GetVadU - BAD VERSION\n");
				VMMDLL_MemFree(pVadMap); pVadMap = NULL;
				return {};
			}
			{
				for (DWORD i = 0; i < pVadMap->cMap; i++) {
					pVadMapEntry = &pVadMap->pMap[i];

					// 从lpAddress开始
					if (pVadMapEntry->vaStart < lpAddress) continue;

					MY_MEMORY_BASIC_INFORMATION64 item = {
						pVadMapEntry->vaStart,
						pVadMapEntry->vaEnd + 1,
						pVadMapEntry->Protection,
						pVadMapEntry->fPrivateMemory,
						pVadMapEntry->MemCommit
					};
					resultset.push_back(item);
				}
				VMMDLL_MemFree(pVadMap); pVadMap = NULL;
			}
			return resultset;
		}

		VOID VadMap_Protection(_In_ MY_MEMORY_BASIC_INFORMATION64& pVad, _Out_writes_(6) LPSTR sz)
		{
			BYTE vh = (BYTE)pVad.Protection >> 3;
			BYTE vl = (BYTE)pVad.Protection & 7;
			sz[0] = pVad.fPrivateMemory ? 'p' : '-';                                    // PRIVATE MEMORY
			sz[1] = (vh & 2) ? ((vh & 1) ? 'm' : 'g') : ((vh & 1) ? 'n' : '-');         // -/NO_CACHE/GUARD/WRITECOMBINE
			sz[2] = ((vl == 1) || (vl == 3) || (vl == 4) || (vl == 6)) ? 'r' : '-';     // COPY ON WRITE
			sz[3] = (vl & 4) ? 'w' : '-';                                               // WRITE
			sz[4] = (vl & 2) ? 'x' : '-';                                               // EXECUTE
			sz[5] = ((vl == 5) || (vl == 7)) ? 'c' : '-';                               // COPY ON WRITE
			if (sz[1] != '-' && sz[2] == '-' && sz[3] == '-' && sz[4] == '-' && sz[5] == '-') { sz[1] = '-'; }
		}

		inline BOOL IsRead(DWORD Protection)
		{
			BYTE vl = (BYTE)Protection & 7;
			return ((vl == 1) || (vl == 3) || (vl == 4) || (vl == 6));
		}

		inline BOOL IsWrite(DWORD Protection)
		{
			BYTE vl = (BYTE)Protection & 7;
			return (vl & 4);
		}

		BOOL IsReadWrite(DWORD Protection)
		{
			return  IsRead(Protection) && IsWrite(Protection);
		}

		DWORD64 MyFindPatternExReg(BYTE* pattern, std::string mask, DWORD64 RegionSize)
		{
			std::vector<MY_MEMORY_BASIC_INFORMATION64> result = MyVirtualQueryEx(0x7ffe0000);
			if (result.empty()) {
				printf("MyVirtualQueryEx 0\n");
				return 0;
			}

			int size = result.size();
			for (int i = 0; i < size; i++)
			{
				auto& mbi = result[i];
				// CHAR szVadProtection[7] = { 0 };
				// VadMap_Protection(mbi, szVadProtection);
				// std::cout << std::hex << mbi.vaStart << "->" << mbi.vaEnd << "protection: " << szVadProtection << std::endl;

				// if (!IsReadWrite(mbi.Protection)) continue;

				__try {
					std::vector<DWORD64> arr = FindPatternEx(mbi.vaStart, mbi.vaEnd, pattern, mask, RegionSize);
					if (arr.size() > 0)
						return arr[0];
				}
				__except (1) {}

			}
			return 0;
		}

		__forceinline std::vector<DWORD64> FindPatternExRegs(BYTE* pattern, std::string mask, DWORD64 RgSize)
		{
			std::vector<DWORD64> Result;
			if (!UpdateMemoryQuery())
				return Result;

			for (int i = 0; i < mbis.size(); i++) {
				MEMORY_BASIC_INFORMATION64 info = mbis[i];

				std::vector<DWORD64> arr = FindPatternEx(info.BaseAddress, info.RegionSize + info.BaseAddress, pattern, mask, RgSize);
				if (arr.size() > 0)
					Result.push_back(arr[0]);
			}

			return Result;
		}

		__forceinline std::vector<DWORD64> FindPatternsExReg(BYTE* pattern, std::string mask, DWORD64 RgSize)
		{
			std::vector<DWORD64> Result;
			DWORD64 EntityStart = FindPatternExReg(pattern, mask, RgSize);
			if (EntityStart)
			{
				for (int i = 0; i < mbis.size(); i++) {
					if (mbis[i].BaseAddress < EntityStart && EntityStart - mbis[i].BaseAddress < mbis[i].RegionSize) {
						EntityStart = mbis[i].BaseAddress;
					}
				}

				BYTE* buf = new BYTE[RgSize];
				ReadArray(EntityStart, buf, RgSize);

				std::vector<DWORD64> Pointers = FindPatterns(buf, pattern, mask, RgSize);
				delete[] buf;

				for (int i = 0; i < Pointers.size(); i++)
					Pointers[i] += EntityStart;

				Result = Pointers;
			}

			return Result;
		}

		__forceinline std::vector<DWORD64> FindPatternsExRegs(BYTE* pattern, std::string mask, DWORD64 RgSize)
		{
			std::vector<DWORD64> Result;
			std::vector<DWORD64> StartPointers = FindPatternExRegs(pattern, mask, RgSize);

			for (int i = 0; i < StartPointers.size(); i++)
			{
				for (int j = 0; j < mbis.size(); j++) {
					if (mbis[j].BaseAddress < StartPointers[i] && StartPointers[i] - mbis[j].BaseAddress < mbis[j].RegionSize) {
						StartPointers[i] = mbis[j].BaseAddress;
					}
				}

				BYTE* buf = new BYTE[RgSize];
				ReadArray<BYTE>(StartPointers[i], buf, RgSize);

				std::vector<DWORD64> Pointers = FindPatterns(buf, pattern, mask, RgSize);
				delete[] buf;

				for (int j = 0; j < Pointers.size(); j++)
					Pointers[j] += StartPointers[i];

				for (int j = 0; j < Pointers.size(); j++)
				{
					Result.push_back(Pointers[j]);
				}
			}
			return Result;
		}
	};
	inline auto SDK = std::make_unique<MemorySDK>();
}