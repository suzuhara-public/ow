#pragma once
#include "get_component.hpp"
#include <complex>

namespace Sendinput
{
	void SendVKcodes(BYTE vk)
	{
		INPUT input;
		ZeroMemory(&input, sizeof(INPUT));
		input.type = INPUT_KEYBOARD;
		input.ki.wVk = vk;

		SendInput(1, &input, sizeof(INPUT));

		input.ki.dwFlags = KEYEVENTF_KEYUP;

		SendInput(1, &input, sizeof(INPUT));
	}
}

namespace OW {
	inline void SetKey(uint32_t key) { // key down
		__try {
			SDK->WPM<uint32_t>(SDK->g_player_controller + 0x1244, key);
		}
		__except (EXCEPTION_EXECUTE_HANDLER) {}
	}

	inline Vector3 SmoothLinear(Vector3 LocalAngle, Vector3 TargetAngle, float speed)
	{
		float d = LocalAngle.DistTo(TargetAngle);

		Vector3 Result;
		Result.X = LocalAngle.X + (TargetAngle.X - LocalAngle.X) * speed;
		Result.Y = LocalAngle.Y + (TargetAngle.Y - LocalAngle.Y) * speed;
		Result.Z = LocalAngle.Z + (TargetAngle.Z - LocalAngle.Z) * speed;
		return Result;
	}

	inline void SetKeyHold(int Key, float duration) {
		clock_t previous = clock();
		while (clock() - previous < duration) {
			SDK->WPM<uint32_t>(SDK->g_player_controller + 0x1244, Key);
		}
	}

	inline void AimCorrection(Vector3* InVecArg, Vector3 currVelocity, float Distance, float Bulletspeed) {
		if (settings::aimbot::hanzo_flick) {
			float m_time = powf(Distance / Bulletspeed, 2.f);
			(*InVecArg).X = (*InVecArg).X + ((currVelocity.X) * (Distance / (Bulletspeed)));
			(*InVecArg).Y = (*InVecArg).Y + ((currVelocity.Y) * (Distance / (Bulletspeed)));
			(*InVecArg).Z = (*InVecArg).Z + ((currVelocity.Z) * (Distance / (Bulletspeed)));
			(*InVecArg).Y += (0.5f * 9.8f * m_time);
		}
		else {
			(*InVecArg).X = (*InVecArg).X + ((currVelocity.X) * (Distance / (Bulletspeed)));
			(*InVecArg).Y = (*InVecArg).Y + ((currVelocity.Y) * (Distance / (Bulletspeed)));
			(*InVecArg).Z = (*InVecArg).Z + ((currVelocity.Z) * (Distance / (Bulletspeed)));
		}
	}

	void SolveQuartic(const std::complex<float> coefficients[5], std::complex<float> roots[4]) {
		const std::complex<float> a = coefficients[4];
		const std::complex<float> b = coefficients[3] / a;
		const std::complex<float> c = coefficients[2] / a;
		const std::complex<float> d = coefficients[1] / a;
		const std::complex<float> e = coefficients[0] / a;

		const std::complex<float> Q1 = c * c - 3.f * b * d + 12.f * e;
		const std::complex<float> Q2 = 2.f * c * c * c - 9.f * b * c * d + 27.f * d * d + 27.f * b * b * e - 72.f * c * e;
		const std::complex<float> Q3 = 8.f * b * c - 16.f * d - 2.f * b * b * b;
		const std::complex<float> Q4 = 3.f * b * b - 8.f * c;

		const std::complex<float> Q5 = std::pow(Q2 / 2.f + std::sqrt(Q2 * Q2 / 4.f - Q1 * Q1 * Q1), 1.f / 3.f);
		const std::complex<float> Q6 = (Q1 / Q5 + Q5) / 3.f;
		const std::complex<float> Q7 = 2.f * std::sqrt(Q4 / 12.f + Q6);

		roots[0] = (-b - Q7 - std::sqrt(4.f * Q4 / 6.f - 4.f * Q6 - Q3 / Q7)) / 4.f;
		roots[1] = (-b - Q7 + std::sqrt(4.f * Q4 / 6.f - 4.f * Q6 - Q3 / Q7)) / 4.f;
		roots[2] = (-b + Q7 - std::sqrt(4.f * Q4 / 6.f - 4.f * Q6 + Q3 / Q7)) / 4.f;
		roots[3] = (-b + Q7 + std::sqrt(4.f * Q4 / 6.f - 4.f * Q6 + Q3 / Q7)) / 4.f;
	}

	Vector3 predictCalc(Vector3 EnPos, float Speed, Vector3 Velocity)
	{
		__try {
			Vector3 Predict;
			auto origin = viewMatrix.GetCameraVec();
			origin.X -= 0.5;
			origin.Y -= 0.85;
			origin.Z -= 0.5;
			double G = -9.81f;
			double A = origin.X;
			double B = origin.Y;
			double C = origin.Z;
			double M = EnPos.X;
			double N = EnPos.Y;
			double O = EnPos.Z;
			double P = Velocity.X * 1000;
			double Q = Velocity.Y * 1000;
			double R = Velocity.Z * 1000;
			double S = Speed;
			double H = M - A;
			double J = O - C;
			double K = N - B;
			double L = -.5f * G;
			double c4 = L * L;
			double c3 = -2 * Q * L;
			double c2 = (Q * Q) - (2 * K * L) - (S * S) + (P * P) + (R * R);
			double c1 = (2 * K * Q) + (2 * H * P) + (2 * J * R);
			double c0 = (K * K) + (H * H) + (J * J);

			std::complex<float> pOutRoots[4];
			const std::complex<float> pInCoeffs[5] = { c0, c1, c2, c3, c4 };
			SolveQuartic(pInCoeffs, pOutRoots);
			float fBestRoot = FLT_MAX;
			for (int i = 0; i < 4; i++) {
				if (pOutRoots[i].real() > 0.f && std::abs(pOutRoots[i].imag()) < 0.0001f && pOutRoots[i].real() < fBestRoot) {
					fBestRoot = pOutRoots[i].real();
				}
			}

			Predict.X = EnPos.X + (Velocity.X * fBestRoot * 1000);
			Predict.Y = EnPos.Y + (Velocity.Y * fBestRoot * 1000);
			Predict.Z = EnPos.Z + (Velocity.Z * fBestRoot * 1000);
			return Predict;
		}
		__except (0) {

		}
	}

	inline bool in_range(Vector3 MyAngle, Vector3 EnemyAngle, Vector3 MyPosition, Vector3 EnemyPosition, float radius)
	{
		float dist = MyPosition.DistTo(EnemyPosition);
		radius /= dist;
		Vector3 sub = MyAngle - EnemyAngle;

		return MyAngle.DistTo(EnemyAngle) <= radius;
	}

	inline Vector3 SmoothAccelerate(Vector3 LocalAngle, Vector3 TargetAngle, float Speed, float Acc)
	{
		Vector3 Result = LocalAngle;
		__try
		{
			Vector3 delta = TargetAngle - LocalAngle;

			float tmp = Acc / delta.get_length();
			tmp = tmp * tmp * 0.005f;

			float c = Speed;
			float chu = tmp * Speed;
			c += chu;

			if (c >= 1) c = 1;

			Result.X += delta.X * c;
			Result.Y += delta.Y * c;
			Result.Z += delta.Z * c;

			return Result;
		}
		__except (1)
		{
			return Result;
		}
	}

	inline XMFLOAT3 CalcAngle(XMFLOAT3 Target, XMFLOAT3 CameraPos) { 
		XMFLOAT3 Result;
		float Distance = XMVectorGetX(XMVector3Length(XMLoadFloat3(&Target) - XMLoadFloat3(&CameraPos)));
		XMStoreFloat3(&Result, (XMLoadFloat3(&Target) - XMLoadFloat3(&CameraPos)) / Distance);
		return Result;
	}

	float GetLookUpSkill(uint16_t a1) // for get Hanzo's Projectile Speed
	{
		__try
		{
			uint64_t pSkill = SDK->RPM<uint64_t>(local_entity.SkillBase + 0x1848);
			uint64_t SkillRawList = SDK->RPM<uint64_t>(pSkill + 0x10);
			uint32_t SkillSize = SDK->RPM<uint32_t>(pSkill + 0x18);
			for (uint32_t i = 0x0; i < SkillSize; i++)
			{
				if (SDK->RPM<uint16_t>(SkillRawList + (i * 0x80)) == a1)
					if (SDK->RPM<float>(SkillRawList + (i * 0x80) + 0x30) >= 0.5f) // 50%
						return SDK->RPM<float>(SkillRawList + (i * 0x80) + 0x30);
			}
		}
		__except (1) {}
		return 0.f;
	}

	Vector3 GetVector3(bool prediction) {
		int TarGetIndex = -1;
		Vector3 target{};
		// Vector2 CrossHair = Vector2(GetSystemMetrics(SM_CXSCREEN) / 2.0f, GetSystemMetrics(SM_CYSCREEN) / 2.0f);
		Vector2 CrossHair = { width / 2.0f, height / 2.0f };

		float origin = 100000.f;

		if (TarGetIndex == -1) {
			if (entities.size() > 0) {
				for (int i = 0; i < entities.size(); i++) {
					if (entities[i].Alive && entities[i].Team && entities[i].Vis)
					{
						Vector3 PredictPos = entities[i].head_pos;
						Vector3 RootPos = entities[i].head_pos;
						Vector3 Vel = entities[i].velocity;

						if (prediction) {
							auto distance = viewMatrix.GetCameraVec().Distance(PredictPos);
							AimCorrection(&PredictPos, Vel, distance, settings::aimbot::prediction_level);
						}

						Vector2 Vec2 = prediction ? viewMatrix.WorldToScreen(PredictPos) : viewMatrix.WorldToScreen(RootPos);
						float CrossDist = CrossHair.Distance(Vec2);

						if (CrossDist < origin && CrossDist <= settings::aimbot::fov) {
							target = prediction ? PredictPos : RootPos;
							origin = CrossDist;
							TarGetIndex = i;
						}
						else {
							TarGetIndex = -1;
						}
					}
					else {
						TarGetIndex = -1;
					}
				}
			}
			return target;
		}
	}
}