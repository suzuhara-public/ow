#pragma once
#include "keybind.hpp"

ImFont* logo;
using namespace OW;

namespace menu {
	namespace defines {
		int current_section = 0;
	}

	namespace utils {
		void draw_wallhack() {
			// move to next child
			ImGui::SetCursorPos(ImVec2(185, 50));

			// draw main child
			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.10f, 0.10f, 0.10f, 1.00f));
			ImGui::BeginChild("Players", ImVec2(175, 300));
			{
				ImGui::Text("Players");
				ImGui::Separator();
				ImGui::Checkbox("2D Box", &settings::wallhack::box);
				ImGui::Checkbox("Distance", &settings::wallhack::distance);
				ImGui::Checkbox("Hero Name", &settings::wallhack::heroname);
				ImGui::Checkbox("Health Bar", &settings::wallhack::health);
				ImGui::Checkbox("Only Visible", &settings::wallhack::only_visible);
				ImGui::Checkbox("Visibility Check", &settings::wallhack::visible_check);
			}
			ImGui::EndChild();
			ImGui::PopStyleColor();

			// move to next child
			ImGui::SetCursorPos(ImVec2(385, 50));

			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.10f, 0.10f, 0.10f, 1.00f));
			ImGui::BeginChild("Outline", ImVec2(175, 300));
			{
				ImGui::Text("Outline");
				ImGui::Separator();
				ImGui::Checkbox("Outline Players", &settings::wallhack::outline_players);

				ImGui::Spacing();
				ImGui::Text("Options");
				ImGui::Separator();
				const char* items[2] = { ("2D Box"), ("Corner Box") };
				ImGui::Text("Box Type");
				ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
				ImGui::Combo("##Box Type", &settings::wallhack::box_type, items, IM_ARRAYSIZE(items));
			}
			ImGui::EndChild();
			ImGui::PopStyleColor();
		}

		void draw_aimbot() {
			// move to next child
			ImGui::SetCursorPos(ImVec2(185, 50));

			// draw main child
			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.10f, 0.10f, 0.10f, 1.00f));
			ImGui::BeginChild("Main", ImVec2(175, 330));
			{
				ImGui::Text("Main");
				ImGui::Separator();
				ImGui::Checkbox("Aimbot", &settings::aimbot::aimbot);
				ImGui::Checkbox("Flick", &settings::aimbot::flick);
				ImGui::Checkbox("Triggerbot", &settings::aimbot::triggerbot);
				

				ImGui::Text("Aimkey");
				aimbot_hotkey(&settings::aimbot::aimkey);

				ImGui::Text("Flickkey");
				flick_hotkey(&settings::aimbot::flickkey);

				ImGui::Text("Aim Smoothing");
				ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
				ImGui::SliderFloat("##Aim Smoothing", &settings::aimbot::aimbot_smooth, 0.1f, 10.0f);

				ImGui::Text("Flick Smoothing");
				ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
				ImGui::SliderFloat("##Flick Smoothing", &settings::aimbot::flick_smooth, 0.1f, 10.0f);
			}
			ImGui::EndChild();
			ImGui::PopStyleColor();

			// move to next child
			ImGui::SetCursorPos(ImVec2(385, 50));

			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.10f, 0.10f, 0.10f, 1.00f));
			ImGui::BeginChild("Options", ImVec2(175, 300));
			{
				ImGui::Text("Options");
				ImGui::Separator();
				ImGui::Checkbox("Draw FOV", &settings::wallhack::fov);

				ImGui::Text("FOV Size");
				ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
				ImGui::SliderFloat("##FOV", &settings::aimbot::fov, 10, 1000);

				ImGui::Checkbox("Prediction", &settings::aimbot::prediction);
				if (settings::aimbot::prediction) {
					ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
					ImGui::Combo("##Heros", &settings::aimbot::hero, settings::aimbot::heroes, IM_ARRAYSIZE(settings::aimbot::heroes));
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Hanzo") settings::aimbot::prediction_level = 110.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Genji") settings::aimbot::prediction_level = 60.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Ana") settings::aimbot::prediction_level = 52.2f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Sojourn") settings::aimbot::prediction_level = 160.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Lucio") settings::aimbot::prediction_level = 50.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Eco") settings::aimbot::prediction_level = 75.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Pharah") settings::aimbot::prediction_level = 35.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Orisa") settings::aimbot::prediction_level = 90.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Torb") settings::aimbot::prediction_level = 70.0f;
					if (settings::aimbot::heroes[settings::aimbot::hero] == "Zen") settings::aimbot::prediction_level = 90.0f;
				}

				if (settings::aimbot::triggerbot) {
					ImGui::Text("Triggerbot Options");
					ImGui::Separator();

					ImGui::Text("Delay (ms)");
					ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
					ImGui::SliderFloat("##Delay", &settings::aimbot::trigger_delay, 0.0f, 2500.0f);

					ImGui::Text("Hitbox Size");
					ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
					ImGui::SliderFloat("##Hitbot Scale", &settings::aimbot::trigger_hitbox, 100.0f, 250.0f);
				}
			}
			ImGui::EndChild();
			ImGui::PopStyleColor();
		}

		void draw_misc() {
			ImGui::SetCursorPos(ImVec2(185, 50));

			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.10f, 0.10f, 0.10f, 1.00f));
			ImGui::BeginChild("Misc", ImVec2(175, 300));
			{
				ImGui::Text("Misc");
				ImGui::Separator();

				ImGui::Checkbox("Auto Heal", &settings::misc::auto_heal);
				if (settings::misc::auto_heal) {
					ImGui::Text("Minimum Health");
					ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
					ImGui::SliderFloat("##Min Health", &settings::misc::min_health, 10.0f, 200.0f);
				}

				ImGui::Checkbox("Auto Melee", &settings::misc::auto_melee);
				if (settings::misc::auto_melee) {
					ImGui::Text("Melee key");
					melee_hotkey(&settings::misc::meleekey);
					ImGui::Text("Enemy Distance");
					ImGui::SetNextItemWidth(ImGui::GetWindowSize().x);
					ImGui::SliderFloat("##Min Distance", &settings::misc::min_dist, 0.0f, 2.5f);
				}
			}
			ImGui::EndChild();
			ImGui::PopStyleColor();

			ImGui::SetCursorPos(ImVec2(385, 50));

			ImGui::PushStyleColor(ImGuiCol_ChildBg, ImVec4(0.10f, 0.10f, 0.10f, 1.00f));
			ImGui::BeginChild("Colors", ImVec2(175, 300));
			{
				ImGui::Text("Colors");
				ImGui::Separator();

				ImGui::ColorEdit4("Visible Color", (float*)&settings::color::visible_color, ImGuiColorEditFlags_NoInputs);
				ImGui::ColorEdit4("Invisible Color", (float*)&settings::color::invisible_color, ImGuiColorEditFlags_NoInputs);
				ImGui::ColorEdit4("Team Color", (float*)&settings::color::team_color, ImGuiColorEditFlags_NoInputs);
			}
			ImGui::EndChild();
			ImGui::PopStyleColor();
		}
	}

	void run() {
		if (settings::defines::menu) {
			ImGui::SetNextWindowSize(ImVec2(600, 400));
			ImGui::Begin("Maria", nullptr, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse);
			{
				ImGui::BeginChild("SideBar", ImVec2(150, ImGui::GetContentRegionAvail().y));
				{
					ImGui::PushFont(logo);
					ImGui::SetCursorPos(ImVec2((ImGui::GetWindowSize().x - ImGui::CalcTextSize("Maria").x) * 0.5f, (ImGui::GetWindowSize().y / 10)));
					ImGui::Text("Maria");
					ImGui::PopFont();

					ImGui::SetCursorPosY((ImGui::GetWindowSize().y / 4) + 15);
					if (ImGui::Button("Visuals", ImVec2(ImGui::GetWindowSize().x, 40))) {
						defines::current_section = 0;
					}
					ImGui::SetCursorPosY((ImGui::GetWindowSize().y / 4) + 55);
					if (ImGui::Button("Aimbot", ImVec2(ImGui::GetWindowSize().x, 40))) {
						defines::current_section = 1;
					}
					ImGui::SetCursorPosY((ImGui::GetWindowSize().y / 4) + 95);
					if (ImGui::Button("Misc", ImVec2(ImGui::GetWindowSize().x, 40))) {
						defines::current_section = 2;
					}
				}
				ImGui::EndChild();
				ImGui::SameLine();

				// draw categories
				switch (defines::current_section) {
				case 0:
					utils::draw_wallhack();
					break;
				case 1:
					utils::draw_aimbot();
					break;
				case 2:
					utils::draw_misc();
					break;
				default:
					break;
				}
			}
			ImGui::End();
		}
	}
}